import json
import socket
import sys
import math
# import numpy


#TODO
# - compute switch distance, should be around 102, see http://www.reddit.com/r/HWO/comments/23lorl/inpiecedistance_longer_than_piece_length/


class NoobBot(object):

### GLOBAL VARIABLES ###

# track selection #
  TRACK="finland"
  #TRACK="germany"    
  #TRACK="usa"
  #TRACK="france"      
  CAR_COUNT = 1
  
# physics model #

  #QUAL: qualification mode, which computes all the physics constants
  #RACE: race mode
  MODE='RACE'  
  
  #speedNew = speedOld + (CONST_A*throttleOld + CONST_B*speedOld)
  CONST_A=0
  CONST_B=0
  STATS=[]
  
# various data and constants #
#TODO: should we make these all capitals to make them stand out?
  gametick = 0
  haveSwitchedThisPiece = 0
  isTurboAvailable = 0
  curThrottle = 0
  prevThrottle = 0				
  previousCarData = {}
  distanceTravelled = 0  
  prevSpeed = 0
  prevCarAngle = 0  
  EPS = 1e-9
    
    
  def __init__(self, socket, name, key):
    # set some global variables				
    self.socket = socket
    self.name = name
    self.key = key
    

  def msg(self, msg_type, data):
    self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.gametick}))

  def send(self, msg):
    self.socket.send((msg + "\n").encode('utf-8'))

  def join(self):
    if self.TRACK == "finland":
      return self.msg("join", {"name": self.name, "key": self.key})
    else:
      return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": self.TRACK, "carCount": self.CAR_COUNT})

  # example: {"msgType": "throttle", "data": 1.0}
  def throttle(self, throttle):
    self.curThrottle = throttle
    self.changeInThrottle = self.curThrottle-self.prevThrottle
    self.msg("throttle", throttle)


  # Turbo multiplies the power of your car's engine for a short period. The server indicates the availability of turbo using the turboAvailable message.
  #
  # {"msgType": "turboAvailable", "data": {
  #   "turboDurationMilliseconds": 500.0,
  #   "turboDurationTicks": 30,
  #   "turboFactor": 3.0
  # }}
  # You can use your turbo boost any time you like after the server has indicated the availability of it. Just like this.
  # 
  # {"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"}
  # Once you turn it on, you can't turn it off. It'll automatically expire in 30 ticks (or what ever the number was in the turboAvailable message).
  def turbo(self):
    if self.isTurboAvailable == 1:
      self.isTurboAvailable = 0
      print("BOT: Turbo engaged!")
      self.msg("turbo", "Vrrrooooooooom!")
      
  # example: {"msgType": "switchLane", "data": "Left"}    
  def switch(self, mydir):
    if self.haveSwitchedThisPiece == 0:
      self.haveSwitchedThisPiece = 1
      self.msg("switchLane", mydir)
      # print 'switching',mydir

  def ping(self):
    self.msg("ping", {})

  def run(self):
    self.join()
    self.msg_loop()

  def on_join(self, data):
    print("BOT: Joined")
    # print(data)
    self.ping()

  def on_game_start(self, data):
    print("BOT: Race started")
    self.ping()
      
  def on_game_init(self, data):
    print("BOT: in game init")
    print(data)
    self.ping()    
      
    # set some global variables        
    self.raceData = data  # contains all data about the race
      
  def on_crash(self, data):
    print("BOT: Someone crashed")
    self.ping()

  def on_game_end(self, data):
    print("BOT: Race ended")
    self.ping()

  def on_error(self, data):
    print("Error: {0}".format(data))
    self.ping()

  def on_turbo_available(self, data):
    self.isTurboAvailable = 1
    print("Turbo Available!")
    print("    gameTick=" + str(self.gametick) + " duration=" + str(data['turboDurationMilliseconds']) + "ms " + str(data['turboDurationTicks']) + " ticks, factor=" + str(data['turboFactor']))

  def msg_loop(self):
    msg_map = {
        'join': self.on_join,
        'gameStart': self.on_game_start,
        'gameInit': self.on_game_init,
        'carPositions': self.on_car_positions,
        'crash': self.on_crash,
        'gameEnd': self.on_game_end,
        'error': self.on_error,
        'turboAvailable': self.on_turbo_available,
    }
    socket_file = s.makefile()
    line = socket_file.readline()
    while line:
      msg = json.loads(line)
      msg_type, data = msg['msgType'], msg['data']
      if 'gameTick' in msg:
        self.gametick = msg['gameTick']            
      if msg_type in msg_map:
        #print("Got {0}".format(msg_type) + " tick=" + str(self.gametick) + " th=" + str(self.curThrottle) + " d=" + str(self.distanceTravelled) + " sp=" + str(self.prevSpeed))
        msg_map[msg_type](data)
      else:
        print("Got {0}".format(msg_type) + " tick=" + str(self.gametick) + " time=" + str(self.gametick / 60.0))
        self.ping()
      line = socket_file.readline()        
      
### GET METHODS ###
  
  
# raceData #

  def doesPieceHaveAttribute(self, pieceID, attribute):    
    return (attribute in self.raceData['race']['track']['pieces'][pieceID])
            
  def isCornerPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'angle')
    
  def isStraightPiece(self, pieceID):
    return not(self.isCornerPiece(pieceID))
    
  # Additionally, there may be a switch attribute that indicates that this
  # piece is a switch piece, i.e. you can switch lanes here.      
  def isSwitchPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'switch')      
    
  def getPieceAttribute(self, pieceID, attribute):
    return self.raceData['race']['track']['pieces'][pieceID][attribute]
    
  # NOTE: assume piece is a corner piece  
  # The angle attribute is the angle of the circular segment that the
  # piece forms. For left turns, this is a negative value. For a 45-degree
  # bend to the left, the angle would be -45. For a 30-degree turn to the
  # right, this would be 30.    
  def getPieceAngle(self, pieceID):      
    return self.getPieceAttribute(pieceID, 'angle')
              
  def getNextPieceID(self, pieceID):
    return (pieceID + 1) % self.getNumPieces()
    
  def getNumPieces(self):
    return len(self.raceData['race']['track']['pieces'])  
    
  # For a Bend, there's radius that depicts the radius of the circle
  # that would be formed by this kind of pieces. More specifically,
  # this is the radius at the center line of the track.      
  def getPieceRadius(self, pieceID):
    return self.getPieceAttribute(pieceID, 'radius')
    
  # NOTE: assume lanes are in increasing order of lane index
  # The track may have 1 to 4 lanes. The lanes are described in
  # the protocol as an array of objects that indicate the lane's
  # distance from the track center line. A positive value tells
  # that the lanes is to the right from the center line while a
  # negative value indicates a lane to the left from the center.
  def getLaneDistFromCenter(self, lane):
    return self.raceData['race']['track']['lanes'][lane]['distanceFromCenter']
          
  # NOTE: assume piece is straight
  # For a Straight the length attribute depicts the length of the piece.
  def getPieceLength(self, pieceID):
    return self.getPieceAttribute(pieceID, 'length')
    
    
  # NOTE: assume piece is curved
  def getCurveLength(self, pieceID, lane):
    angle = self.getPieceAngle(pieceID)
    length = self.getCurveRadius(pieceID, lane) * 2 * math.pi * abs(angle) / 360.0
    assert length >= 0
    
    return length

    
  # NOTE: assume piece is curved
  def getCurveRadius(self, pieceID, lane):    
    angle = self.getPieceAngle(pieceID)
    mult = -1
    if angle < 0: mult = +1  # left turn
    radius = self.getPieceRadius(pieceID) + mult * self.getLaneDistFromCenter(lane)
    assert radius >= 0

    return radius    
    
# carData #

  # TODO: assumes just 1 player
  # zero based index of the piece the car is on
  def getPieceID(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['pieceIndex']              
    else:
      return data[0]['piecePosition']['pieceIndex']           
          
    
  # TODO: assumes just 1 player      
  # the distance the car's Guide Flag (see above) has travelled from the
  # start of the piece along the current lane    
  # So I guess no need to complex computations for curves?    
  def getInPieceDistance(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['inPieceDistance']          
    else:
      return data[0]['piecePosition']['inPieceDistance']    
    
    
  # TODO: assumes just 1 player     
  # the number of laps the car has completed. The number 0 indicates
  # that the car is on its first lap. The number -1 indicates that it
  # has not yet crossed the start line to begin it's first lap.       
  def getLap(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['lap']      
    else:
      return data[0]['piecePosition']['lap']
             
    
  # TODO: assumes just 1 player, assume startLane==endLane
  # a pair of lane indices. Usually startLaneIndex and endLaneIndex
  # are equal, but they do differ when the car is currently switching lane    
  def getLane(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['lane']['startLaneIndex']      
    else:
      return data[0]['piecePosition']['lane']['startLaneIndex']
    
          
  # TODO: assumes just 1 player  
  # The angle depicts the car's slip angle. Normally this is zero, but
  # when you go to a bend fast enough, the car's tail will start to drift.
  # Naturally, there are limits to how much you can drift without crashing
  # out of the track.            
  def getCarAngle(self, data={}):
    if data == {}:
      return self.carData[0]['angle']                
    else:      
      return data[0]['angle']          
    
    
    
  # distance travelled since last tick
  def getDistanceTravelled(self):  
  
    # start of race, there is no distance travelled yet
    if self.previousCarData == {}: return 0

    curPieceID = self.getPieceID()
    previousPieceID = self.getPieceID(self.previousCarData)
  
    curPieceDistance = self.getInPieceDistance()      
    previousPieceDistance = self.getInPieceDistance(self.previousCarData)
    
  
    previousLane = self.getLane(self.previousCarData)
  
    distToStartOfCurPiece = curPieceDistance      
    distToStartOfPreviousPiece = previousPieceDistance        
  
    if self.isCornerPiece(previousPieceID):    
      prevPieceLength = self.getCurveLength(previousPieceID, previousLane)    
    else:
      prevPieceLength = self.getPieceLength(previousPieceID)
    
    distToEndOfPreviousPiece = prevPieceLength - distToStartOfPreviousPiece        
    #IF THIS ASSERT FAILS then there is something wrong... please record where it fails
    # fails after pre-corner pieceID = 17 on USA (lap=0 tick=282 d=1660.6917330181439) 
    # note server reported piece distance measurements are all based on the centre line-length
    # using lane-length calculations might be causing error.
    assert distToEndOfPreviousPiece >= 0
  
    # if on the same piece
    if curPieceID == previousPieceID:
      assert distToStartOfCurPiece - distToStartOfPreviousPiece >= 0
      return distToStartOfCurPiece - distToStartOfPreviousPiece
     
    # on a new track piece
    self.haveSwitchedThisPiece = 0
    return distToStartOfCurPiece + distToEndOfPreviousPiece
    

### AI METHODS ###

  def on_car_positions(self, data):
  
    self.carData = data  
    # print(data)
  
    self.updateBeforeThrottle()
    if self.MODE=='QUAL':
      self.doQualification()
    elif self.MODE=='RACE':      
      self.AI_fixed()
      # self.AI_simple8()
    self.updateAfterThrottle()
    
    
   
     
     
#############################################################################
    
    
  def doQualification(self):
    
    pieceID = self.getPieceID()    

    #set a small constant throttle    
    self.throttle(0.2)                
            
    if self.isStraightPiece(pieceID) and abs(self.getCarAngle()) < self.EPS and self.prevSpeed > self.EPS:
      self.STATS.append({'s':self.prevSpeed, 'a':self.acceleration, 't':self.prevThrottle})      
            
    if len(self.STATS)==2:    
      print(self.STATS)
      st1=self.STATS[0]
      st2=self.STATS[1]            
      
      #using numpy
#       a = numpy.array([[st1['t'],st1['s']],[st2['t'],st2['s']]])
#       b = numpy.array([st1['a'],st2['a']])
#       x = numpy.linalg.solve(a, b)            
      self.CONST_A=x[0]
      self.CONST_B=x[1]
      
      #using a toy matrix library
      #a = matrix.matrix([[st1['t'],st1['s']],[st2['t'],st2['s']]])
      #b = matrix.vector([st1['a'],st2['a']])
      #x = matrix.solve(a,b)
      #self.CONST_A=x.get((0,0))
      #self.CONST_B=x.get((1,0))      
      
      #using my brain :)
      #self.CONST_B=(st2['a']*st1['t']-st1['a']*st2['t'])/(st2['s']*st1['t']-st1['s']*st2['t'])
      #self.CONST_A=(st1['a']-self.CONST_B*st1['s'])/st1['t']      
      print("tick=" + str(self.gametick) + ", speedNew = speedOld + " + str(self.CONST_A)+"*throttleOld + " + str(self.CONST_B)+"*speedOld")
      self.MODE='RACE'
      
    
    
    
  def updateBeforeThrottle(self):
    self.dist = self.getDistanceTravelled()  # distance travelled since last tick
    self.distanceTravelled += self.dist      # total distance travelled
    self.speed = self.dist / 1               # instantaneous speed. NOTE: assume we didn't miss any ticks
    self.acceleration = (self.speed - self.prevSpeed) / 1
    # average speed    
    if self.gametick == 0:
      self.averageSpeed = 0        
    else:
      self.averageSpeed = self.distanceTravelled / self.gametick  
      
      
    #print 'tick',self.gametick,'dist',self.dist,'speed',self.speed,'accel',self.acceleration

    pieceID = self.getPieceID()    
    
    # find relationship between throttle, speed and acceleration on straight pieces  
    #if self.isStraightPiece(pieceID) and abs(self.getCarAngle()) < self.EPS:
      #print self.prevSpeed, self.acceleration
      
    previousPieceID = self.getPieceID(self.previousCarData)
    previousLane = self.getLane(self.previousCarData)     
    self.carAngle=self.getCarAngle()
    self.changeInAngle=self.carAngle-self.prevCarAngle    
  
    if self.isCornerPiece(previousPieceID):        
      angle = self.getPieceAngle(previousPieceID)
      mult = -1
      if angle < 0: mult = +1  # left turn
      prevRadius = self.getPieceRadius(previousPieceID) + mult * self.getLaneDistFromCenter(previousLane)
      #print self.prevCarAngle, self.changeInAngle, prevRadius
    
    
  def updateAfterThrottle(self):
    self.previousCarData = self.carData      
    self.prevSpeed = self.speed
    self.prevCarAngle = self.carAngle
    self.prevThrottle = self.curThrottle
    
    
  #drive at a constant speed
  #constant speed occurs when A*throttle+B*speed=0
  def AI_constantSpeed(self):
    speed=5
    th=-self.CONST_B*speed/self.CONST_A
    self.throttle(th)
      
    
  # th=0.5, time=12.75 + CRASH (GERMANY)
  def AI_basic(self):      
    th = 0.6
    self.throttle(th)      

  # One throttle for straight pieces and one for corners
  # cornerThrottle=0.6, straightThrottle=0.75, time=9.07
  # cornerThrottle=0.6, straightThrottle=0.7, time=9.27
  # cornerThrottle=0.65, straightThrottle=0.7, CRASH
  # cornerThrottle=0.6, straightThrottle=0.8, CRASH   
  def AI_simple(self):
  
    cornerThrottle = 0.6
    straightThrottle = 0.7
    
    pieceID = self.getPieceID()
    
    if self.isCornerPiece(pieceID):
      self.throttle(cornerThrottle)
    else:
      self.throttle(straightThrottle)          
            
  # simple with lane switching          
  # cornerThrottle=0.6, straightThrottle=0.7, time=9.03 + CRASH              
  def AI_simple2(self):

    cornerThrottle = 0.6
    straightThrottle = 0.7
    
    pieceID = self.getPieceID()
    
    if self.isCornerPiece(pieceID):
      self.throttle(cornerThrottle)
    else:
      self.throttle(straightThrottle)
      
    # switch lanes if it is faster  
    nextPieceID = self.getNextPieceID(pieceID)        
    if self.isSwitchPiece(nextPieceID):
      nextPieceID2 = self.getNextPieceID(nextPieceID)
      if self.isCornerPiece(nextPieceID2):
        # next piece turns left          
        if self.getPieceAngle(nextPieceID2) < 0:   
          self.switch('Left')
        else:
          self.switch('Right')          
                              
            
            
  # speed up when angle is good, slow down when angle is bad
  # up=0.01, down=0.1, max=0.71, min=0.55, time=9.12    
  # up=0.02, down=0.1, max=0.7, min=0.55, time=9.17
  # up=0.01, down=0.1, max=0.7, min=0.55, time=9.18              
  # up=0.01, down=0.1, max=0.7, min=0.5, time=9.45
  # up=0.01, down=0.1, max=0.75, min=0.3, time=10.15
  # up=0.01, down=0.01, max=0.7, min=0.55, CRASH (need to speed down faster!)     
  # up=0.01, down=0.1, max=0.71, min=0.57, CRASH
  # up=0.02, down=0.1, max=0.73, min=0.55, CRASH    
  # up=0.01, down=0.1, max=0.75, min=0.5, CRASH
  # up=0.01, down=0.1, max=0.75, min=0.45, CRASH
  # up=0.01, down=0.1, max=0.75, min=0.4, CRASH    
  # up=0.01, down=0.1, max=0.7, min=0.6, CRASH
  def AI_simple4(self):
  
    speedUp = 0.01
    speedDown = 0.1
    maxSpeed = 0.70
    minSpeed = 0.55
    angleThresh = 5     
  
    angle = self.getCarAngle()      
    if abs(angle) <= angleThresh:
      self.curThrottle = min(maxSpeed, self.curThrottle + speedUp)
    else:
      self.curThrottle = max(minSpeed, self.curThrottle - speedDown)
  
    self.throttle(self.curThrottle)      
    

  # go fast on straight pieces, break before corners
  # pre=0.5, corner=0.67, straight=0.81, time=8.58 + CRASH 
  # pre=0.5, corner=0.67, straight=0.8, time=8.62 
  # pre=0.5, corner=0.65, straight=0.8, time=8.77  
  # pre=0.35, corner=0.65, straight=0.9, time=8.80 + CRASH
  # pre=0.3, corner=0.65, straight=0.9, time=8.90
  # pre=0.4, corner=0.6, straight=0.9, time=9.08      
  # pre=0.5, corner=0.6, straight=0.8, time=9.17  
  # pre=0.35, corner=0.6, straight=0.9, time=9.20        
  # pre=0.3, corner=0.6, straight=0.9, time=9.35    
  # pre=0.2, corner=0.6, straight=0.95, time=9.45        
  def AI_simple5(self):
  
    preCornerThrottle = 0.50
    cornerThrottle = 0.67
    straightThrottle = 0.8
    
    
    pieceID = self.getPieceID()
    
    if self.isCornerPiece(pieceID):
      self.throttle(cornerThrottle)
    else:
      nextPieceID = self.getNextPieceID(pieceID)  
      if self.isCornerPiece(nextPieceID):
        self.throttle(preCornerThrottle)
      else:
        self.throttle(straightThrottle)          
          
          
  # like AI_simple5, but sprint at the end of the last lap
  # pre=0.5, corner=0.67, straight=0.8, time=8.40     
  def AI_simple6(self):
  
    preCornerThrottle = 0.50
    cornerThrottle = 0.67
    straightThrottle = 0.8
    
    
    pieceID = self.getPieceID()
    th = 0
    
    if self.isCornerPiece(pieceID):
      th = cornerThrottle
    else:
      nextPieceID = self.getNextPieceID(pieceID)  
      if self.isCornerPiece(nextPieceID):
        th = preCornerThrottle
      else:
        th = straightThrottle
        
    
    if self.getLap() == 2 and pieceID >= len(self.raceData['race']['track']['pieces']) - 7:
      th = 1
    
    self.throttle(th)
      
      
  # like AI_simple6 but with afterCornerThrottle
  # pre=0.5, corner=0.66, after=0.71, straight=0.8, time=8.37 **********************       
  # pre=0.5, corner=0.67, after=0.68, straight=0.8, time=8.38
  # pre=0.5, corner=0.66, after=0.7, straight=0.8, time=8.38
  # pre=0.5, corner=0.65, after=0.7, straight=0.8, time=8.45
  # pre=0.5, corner=0.67, after=0.7, straight=0.8, CRASH
  def AI_simple7(self):
  
    preCornerThrottle = 0.50
    cornerThrottle = 0.67
    afterCornerThrottle = 0.69
    straightThrottle = 0.8
    
    
    pieceID = self.getPieceID()
    nextPieceID = self.getNextPieceID(pieceID)          
    th = 0
    
    if self.isCornerPiece(pieceID):
      if self.isStraightPiece(nextPieceID):        
        th = afterCornerThrottle
      else:
        th = cornerThrottle
    else:
      if self.isCornerPiece(nextPieceID):
        th = preCornerThrottle
      else:
        th = straightThrottle
        
    
    if self.getLap() == 2 and pieceID >= len(self.raceData['race']['track']['pieces']) - 7:
      th = 1
    
    self.throttle(th)        



  # Like simple7 but go full throttle at the start of the race and fast across the start/finish line
  # Use Turbo on the final lap home straight sprint.
	#preC=0.00, corner=0.74, preS=0.82, straight=0.92, usa	 time=6.984, 6.333, 5.800
  #preC=0.00, corner=0.71, preS=0.79, straight=0.89, usa	 time=7.217, 6.550, 6.000
  #preC=0.00, corner=0.69, preS=0.77, straight=0.87, usa	 time=7.350, 6.717, 6.167
  #preC=0.00, corner=0.67, preS=0.75, straight=0.85, usa	 time=7.434, 6.883, 6.300
  #preC=0.00, corner=0.65, preS=0.75, straight=0.85, usa   time=7.617, 6.967, 6.866
  def AI_simple8(self):
    
    preCornerThrottle = 0.0
    cornerThrottle = 0.74
    preStraightThrottle = 0.82
    straightThrottle = 0.92
    
    pieceID = self.getPieceID()
    nextPieceID = self.getNextPieceID(pieceID)  
   
    # switch lanes if it is faster.
    if self.isSwitchPiece(nextPieceID):
      nextPieceID2 = self.getNextPieceID(nextPieceID)
      if self.isCornerPiece(nextPieceID2):
        # next piece turns left          
        if self.getPieceAngle(nextPieceID2) < 0:   
          self.switch('Left')
        else:
          self.switch('Right')

    # full throttle at the start
    if self.gametick < 68:
      print("race start!")
      self.throttle(1.0)
      return

    # Turbo and full throttle at the final finish (-7 finland, -6 usa)
    if self.getLap()==2 and pieceID>=len(self.raceData['race']['track']['pieces'])-6:
      print("final finish turbo sprint " + str(pieceID))
      self.turbo()
      self.throttle(1.0)
      return

    # almost full throttle at the start/finish straight
    if nextPieceID == 0 or pieceID == 0:
      print("start/finish straight " + str(pieceID))
      self.throttle(0.96) #
      return

    if self.isCornerPiece(pieceID) and not self.isCornerPiece(nextPieceID):
      print("pre-straight " + str(pieceID))
      self.throttle(preStraightThrottle)
      return

    if self.isCornerPiece(pieceID):
      print("corner " + str(pieceID))
      self.throttle(cornerThrottle)
      return
    
    if self.isCornerPiece(nextPieceID):
      print("preCorner " + str(pieceID))
      self.throttle(preCornerThrottle)
    else:
      print("straight " + str(pieceID))
#       self.turbo() # causes crash at end of straight when combined with high throttle!
      self.throttle(straightThrottle)    
      
      
      
  def AI_fixed(self):
  
    if self.TRACK=='finland':
      #1480, assumes maxSlip=60, fastest lap 8.07
      #th=[0.9, 1.0, 1.0, 1.0, 0.2, 0.8, 0.6, 0.6, 1.0, 0.5, 0.1, 0.7, 1.0, 0.5, 0.8, 0.3, 0.5, 1.0, 1.0, 0.2, 1.0, 0.5, 1.0, 1.0, 0.5, 1.0, 0.2, 0.7, 0.7, 0.3, 0.9, 0.4, 1.0, 0.5, 1.0, 1.0,1.0, 0.9, 1.0, 1.0,0.3, 1.0, 0.9, 0.9, 0.0, 0.4, 1.0, 0.9, 0.9, 1.0, 1.0, 0.7, 1.0, 0.0, 0.6, 0.0, 1.0, 0.7, 0.6, 0.9, 0.6, 0.7, 1.0, 0.8, 0.7, 0.9, 0.2, 0.8, 0.2, 0.9, 0.3, 0.9, 0.5, 0.9, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9,1.0, 1.0, 0.9, 0.3, 0.0, 1.0, 0.5, 1.0, 1.0, 1.0, 0.8, 1.0, 0.4, 0.4, 0.6, 0.3, 0.6, 1.0, 0.9, 0.5, 0.7, 0.7, 0.9, 1.0, 1.0, 0.8, 0.3, 0.3, 1.0, 0.2, 1.0, 1.0, 0.3, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
      #1453, assume maxSlip=60, crashes :(
      #th=[1.0, 1.0, 1.0, 1.0, 0.3, 0.4, 1.0, 0.8, 0.9, 1.0, 0.8, 0.9, 1.0, 0.4, 0.2, 0.1, 1.0, 0.8, 0.5, 0.5, 0.9, 0.8, 0.7, 1.0, 0.8, 0.7, 0.4, 0.6, 0.6, 0.5, 0.7, 0.9, 0.3, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.1, 0.4, 0.8, 0.5, 0.8, 0.9, 1.0, 1.0, 0.9, 1.0, 1.0, 0.1, 0.3, 0.1, 1.0, 0.7, 0.7, 0.9, 0.5, 0.6, 1.0, 0.9, 1.0, 0.9, 0.2, 0.4, 1.0, 0.2, 0.8, 0.9, 0.3, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.8, 1.0, 0.8, 0.5, 0.6, 0.0, 1.0, 0.7, 0.9, 1.0, 1.0, 1.0, 0.8, 0.7, 0.6, 0.0, 0.2, 1.0, 1.0, 0.9, 0.3, 0.7, 0.7, 1.0, 1.0, 1.0, 0.6, 0.3, 0.9, 0.5, 0.4, 0.6, 1.0, 0.4, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
#   1430
# lap 0 [1.0, 1.0, 0.9, 0.6, 0.8, 0.3, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9, 1.0, 0.1, 0.2, 0.4, 0.5, 1.0, 1.0, 0.4, 0.7, 0.7, 1.0, 1.0, 1.0, 0.6, 0.4, 0.6, 0.9, 0.1, 1.0, 1.0, 0.2, 0.9, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
# lap 1 [1.0, 1.0, 0.8, 0.4, 0.1, 0.8, 0.6, 1.0, 1.0, 1.0, 0.9, 1.0, 1.0, 0.0, 0.3, 0.4, 0.8, 0.7, 1.0, 0.4, 0.8, 0.6, 0.9, 0.8, 1.0, 1.0, 0.2, 0.3, 1.0, 0.3, 1.0, 1.0, 0.1, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9, 1.0]
# lap 2 [1.0, 1.0, 1.0, 0.4, 0.1, 0.4, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.8, 0.2, 0.2, 0.4, 0.8, 0.9, 1.0, 0.4, 0.5, 0.9, 1.0, 1.0, 0.9, 1.0, 0.0, 0.5, 0.8, 0.3, 1.0, 0.9, 0.2, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
      th = [1.0, 1.0, 0.9, 0.6, 0.8, 0.3, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9, 1.0, 0.1, 0.2, 0.4, 0.5, 1.0, 1.0, 0.4, 0.7, 0.7, 1.0, 1.0, 1.0, 0.6, 0.4, 0.6, 0.9, 0.1, 1.0, 1.0, 0.2, 0.9, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.8, 0.4, 0.1, 0.8, 0.6, 1.0, 1.0, 1.0, 0.9, 1.0, 1.0, 0.0, 0.3, 0.4, 0.8, 0.7, 1.0, 0.4, 0.8, 0.6, 0.9, 0.8, 1.0, 1.0, 0.2, 0.3, 1.0, 0.3, 1.0, 1.0, 0.1, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9, 1.0, 1.0, 1.0, 1.0, 0.4, 0.1, 0.4, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.8, 0.2, 0.2, 0.4, 0.8, 0.9, 1.0, 0.4, 0.5, 0.9, 1.0, 1.0, 0.9, 1.0, 0.0, 0.5, 0.8, 0.3, 1.0, 0.9, 0.2, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
    
      
    if self.TRACK=='germany':
      #1731, assumes maxSlip=60, crashes :(      
      #1998, assumes maxSlip=30, fastest lap 10.87
      th=[0.5, 0.7, 0.4, 0.5, 0.8, 0.2, 0.0, 0.2, 0.1, 0.4, 1.0, 0.6, 1.0, 1.0, 0.2, 0.7, 0.3, 0.5, 0.3, 0.9, 1.0, 0.9, 0.1, 0.1, 0.3, 0.7, 0.8, 0.2, 0.5, 0.7, 1.0, 1.0, 0.2, 0.9, 0.6, 0.8, 0.1, 0.8, 0.3, 1.0, 1.0, 0.7, 0.7, 0.4, 0.4, 0.1, 0.6, 0.4, 0.9, 1.0, 0.8, 0.4, 0.6, 1.0, 0.9, 1.0, 1.0,0.4, 0.0, 0.0, 0.7, 0.2, 0.4, 0.1, 0.6, 0.3, 0.2, 0.4, 1.0, 0.6, 0.8, 0.9, 0.2, 0.9, 0.6, 0.2, 0.5, 1.0, 0.4, 0.2, 0.1, 0.7, 1.0, 0.6, 0.1, 0.7, 1.0, 1.0, 1.0, 1.0, 0.3, 0.4, 0.9, 0.0, 0.5, 0.9, 0.8, 1.0, 0.2, 0.8, 0.6, 0.5, 0.1, 0.5, 0.1, 0.5, 0.5, 1.0, 0.9, 0.7, 0.2, 0.6, 0.9, 0.4,0.8, 0.3, 0.1, 0.3, 0.4, 0.3, 0.3, 0.6, 0.3, 0.2, 0.9, 0.5, 0.9, 0.7, 0.9, 0.1, 1.0, 0.3, 0.3, 0.9, 1.0, 0.7, 0.0, 0.5, 0.3, 0.8, 1.0, 0.0, 0.8, 1.0, 1.0, 1.0, 0.8, 0.7, 0.6, 0.5, 0.1, 0.3, 1.0, 0.9, 1.0, 0.8, 0.7, 0.4, 0.4, 0.1, 0.2, 1.0, 0.5, 1.0, 1.0, 0.3, 0.8, 1.0, 1.0, 1.0, 1.0]
      
    if self.TRACK=='usa':      
      #945, assumes maxSlip=60, fastest lap 5.2
      th=[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
      
      
    if self.TRACK=='france':
      #1401, assumes maxSlip=60, crashes :(
      #1567, assumes maxSlip=30, fastest lap 8.58
      th=[1.0, 0.8, 1.0, 0.7, 0.9, 0.6, 0.3, 0.8, 0.4, 0.2, 1.0, 0.5, 0.4, 0.3, 0.2, 0.4, 0.5, 1.0, 1.0, 0.1, 0.3, 0.0, 1.0, 0.4, 1.0, 0.2, 0.8, 0.5, 0.8, 1.0, 1.0, 0.6, 0.2, 0.5, 0.9, 0.7, 1.0, 1.0, 0.9, 0.9, 1.0, 1.0, 0.2, 1.0,1.0, 1.0, 1.0, 0.8, 0.4, 0.3, 0.3, 1.0, 0.7, 0.3, 1.0, 1.0, 0.0, 0.0, 0.5, 0.7, 0.4, 1.0, 0.2, 0.3, 0.5, 0.7, 0.6, 0.2, 0.9, 0.8, 0.2, 0.6, 0.7, 1.0, 0.9, 1.0, 0.5, 1.0, 0.3, 0.6, 0.7, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,0.6, 0.9, 0.7, 0.7, 0.8, 0.1, 0.5, 0.8, 0.8, 0.8, 1.0, 0.5, 0.4, 0.0, 0.1, 0.6, 0.4, 1.0, 0.5, 0.2, 0.5, 0.2, 0.9, 0.9, 0.8, 0.6, 0.2, 0.7, 0.9, 0.9, 1.0, 1.0, 0.1, 1.0, 0.3, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
      


        
    pieceID = self.getPieceID() + self.getLap()*self.getNumPieces()
    if pieceID == len(th) - 5:
      self.turbo()
      
    if pieceID < len(th):
      self.throttle(th[pieceID])
    else:
      self.throttle(1.0)
        


if __name__ == "__main__":
  if len(sys.argv) != 5:
    print("Usage: ./run host port botname botkey")
  else:
    host, port, name, key = sys.argv[1:5]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = NoobBot(s, name, key)
    bot.run()
