import math
import random
import copy

#TODO:
# - multiple sections per piece
# - add switch moves
# - optimize k pieces then play a few pieces then repeat
# - reuse computations from previous laps
# - add turbo

class tester:

  TRACK='finland'
  #TRACK='germany'
  #TRACK='usa'
  #TRACK='france'

  #maxVelocity = const_r1 * radius + const_r2
  CONST_R1=0.0325
  CONST_R2=2.417
  
  #speedNew = speedOld + const_T*throttleOld + const_V*speedOld  
  CONST_T=0.2
  CONST_V=-0.02

  CONST_A1=0.1
  CONST_A2=0.00125
  CONST_M=0.5

  STEP = 0.1    #note assume this divides evenly into 1
  
  MAX_SLIP_ANGLE = 60
  


  if TRACK=='finland':
    raceData={u'race': {u'track': {u'pieces': [{u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 22.5, u'switch': True, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0}, {u'angle': -22.5, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 22.5, u'radius': 200}, {u'angle': -22.5, u'radius': 200}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 62.0}, {u'angle': -45.0, u'switch': True, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 90.0}], u'lanes': [{u'index': 0, u'distanceFromCenter': -10}, {u'index': 1, u'distanceFromCenter': 10}], u'id': u'keimola', u'startingPoint': {u'position': {u'y': -44.0, u'x': -300.0}, u'angle': 90.0}, u'name': u'Keimola'}, u'cars': [{u'id': {u'color': u'red', u'name': u'Ferrari'}, u'dimensions': {u'width': 20.0, u'length': 40.0, u'guideFlagPosition': 10.0}}], u'raceSession': {u'laps': 3, u'maxLapTimeMs': 60000, u'quickRace': True}}}
    
  if TRACK=='germany':
    raceData={u'race': {u'track': {u'pieces': [{u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': -22.5, u'radius': 100}, {u'angle': 45.0, u'radius': 50}, {u'length': 100.0, u'switch': True}, {u'length': 100.0}, {u'angle': -45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 22.5, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'length': 100.0}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': 22.5, u'radius': 100}, {u'length': 100.0}, {u'length': 50.0}, {u'angle': -22.5, u'radius': 100}, {u'angle': -22.5, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 22.5, u'radius': 100}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'angle': 45.0, u'radius': 100}, {u'length': 70.0}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 50.0}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'switch': True, u'radius': 100}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 59.0}], u'lanes': [{u'index': 0, u'distanceFromCenter': -10}, {u'index': 1, u'distanceFromCenter': 10}], u'id': u'germany', u'startingPoint': {u'position': {u'y': 192.0, u'x': -12.0}, u'angle': -90.0}, u'name': u'Germany'}, u'cars': [{u'id': {u'color': u'red', u'name': u'Ferrari'}, u'dimensions': {u'width': 20.0, u'length': 40.0, u'guideFlagPosition': 10.0}}], u'raceSession': {u'laps': 3, u'maxLapTimeMs': 60000, u'quickRace': True}}}

  if TRACK=='usa':
    raceData={u'race': {u'track': {u'pieces': [{u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0}], u'lanes': [{u'index': 0, u'distanceFromCenter': -20}, {u'index': 1, u'distanceFromCenter': 0}, {u'index': 2, u'distanceFromCenter': 20}], u'id': u'usa', u'startingPoint': {u'position': {u'y': -96.0, u'x': -340.0}, u'angle': 90.0}, u'name': u'USA'}, u'cars': [{u'id': {u'color': u'red', u'name': u'Ferrari'}, u'dimensions': {u'width': 20.0, u'length': 40.0, u'guideFlagPosition': 10.0}}], u'raceSession': {u'laps': 3, u'maxLapTimeMs': 60000, u'quickRace': True}}}
  
  if TRACK=='france':
    raceData={u'race': {u'track': {u'pieces': [{u'length': 102.0}, {u'length': 94.0}, {u'angle': 22.5, u'switch': True, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'switch': True, u'radius': 100}, {u'length': 53.0}, {u'angle': -22.5, u'radius': 200}, {u'angle': -45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': -22.5, u'switch': True, u'radius': 200}, {u'length': 94.0}, {u'length': 94.0}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'length': 99.0, u'switch': True}, {u'length': 99.0}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': -45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'angle': 22.5, u'switch': True, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'length': 53.0}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'angle': 45.0, u'radius': 100}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'length': 94.0, u'switch': True}, {u'length': 94.0}, {u'length': 94.0}, {u'length': 94.0}, {u'length': 94.0}, {u'length': 91.0}], u'lanes': [{u'index': 0, u'distanceFromCenter': -10}, {u'index': 1, u'distanceFromCenter': 10}], u'id': u'france', u'startingPoint': {u'position': {u'y': -168.0, u'x': -303.0}, u'angle': -90.0}, u'name': u'France'}, u'cars': [{u'id': {u'color': u'red', u'name': u'Ferrari'}, u'dimensions': {u'width': 20.0, u'length': 40.0, u'guideFlagPosition': 10.0}}], u'raceSession': {u'laps': 3, u'maxLapTimeMs': 60000, u'quickRace': True}}}  

  def __init__(self):
    a=1

  def solve(self):
    print(self.raceData)
    
    laps=3
    n=laps*self.getNumPieces()    #number of pieces to solve
    step = self.STEP
    bestTick=10000
    bestScore=1e20
    loadBest=True
    maxMutations=0
    
    #0 - no optimization, just rely on random mutations
    #1 - change the value of one throttle
    #2 - increase one throttle, while decreasing its neighbour. On its own this seems quite weak
    #3 - do 1 and 2
    optimizeType='1'
    
    startState={'piece':0,'pieceDistance':0,'speed':0,'angle':0,'lane':0,'angleVelocity':0}
    
    #throttles=self.getRandomSmallThrottles(n,step)
    throttles = []
    for i in range(n): throttles.append(step)
    
    bestThrottles=copy.deepcopy(throttles)

    endState=self.simulate(throttles, startState, bestTick)        
    bestScore=self.score(endState)

    numSteps=1/step+1    
    
    
    myiter=0
    while True:
      
      #load the current best solution
      if loadBest: throttles=copy.deepcopy(bestThrottles)       
               
      #perform some mutations
      if myiter>0:
        mutations=int(random.random()*maxMutations+1)
        for i in range(mutations):
          pos = int(random.random()*n)
          throttles[pos] = step * int(random.random()*numSteps)
      
      
      #possibly perform some optimization on throttles
      if optimizeType=='0':
        endState=self.simulate(throttles, startState, bestTick)    
      if optimizeType=='1' or optimizeType=='3':
        endState=self.optimize1(startState, throttles, n, step)
      if optimizeType=='2' or optimizeType=='3':
        endState=self.optimize2(startState, throttles, n, step)
        
      
      
      score=self.score(endState)
      
      if score<=bestScore:
        if not endState['crash']: bestTick=endState['tick']
        
        bestThrottles=copy.deepcopy(throttles)
        
        self.myRound(throttles,2)   #round for nicer output
        
        if score<bestScore:
          print(score)
          for i in range(laps):
            print("lap",i,throttles[i*self.getNumPieces():(i+1)*self.getNumPieces()])
            
        bestScore=score
        
      myiter+=1
    
  
  # a move consists of changing the value of one throttle
  def optimize1(self, startState, throttles, n, step):
  
    bestTick=10000
    numSteps=int(1/step)+1       
    
    bestState=self.simulate(throttles, startState, bestTick)
    bestScore=self.score(bestState)    
    if not bestState['crash']: bestTick=bestState['tick']

    ind = []
    for i in range(n*numSteps):
      ind.append(i)
    
    while True:
      self.shuffle(ind)
      changed=False
      
      for i in ind:
        pos=int(i/numSteps)
        
        #no point optimizing pieces after crash
        if bestState['crash'] and pos > bestState['finishedN']: continue
        
        val=(i%numSteps)*step
        
        if throttles[pos]==val: continue    #same value
        
        #no point decreasing throttle if we are not crashing
        if not(bestState['crash']) and val<=throttles[pos] - self.STEP:
          continue
                  
        #TODO: no point increasing throttle in pieces before the crash?

        oldVal=throttles[pos]
        throttles[pos]=val
        
        endState=self.simulate(throttles, startState, bestTick)        
        score=self.score(endState)
        
        #accept move
        if score<=bestScore:
          if not endState['crash']: bestTick=endState['tick']
          
          if score<bestScore: changed=True
          bestScore=score
          bestState=copy.deepcopy(endState)
        #undo
        else:
          throttles[pos]=oldVal
          
      break
      if not changed: break
      

    return bestState
    
    
    
  # a move consists of changing increasing one throttle, while decreasing its neighbour
  def optimize2(self, startState, throttles, n, step):
  
    bestTick=10000
    numSteps=int(1/step)+1       
    
    bestState=self.simulate(throttles, startState, bestTick)
    bestScore=self.score(bestState)    
    if not bestState['crash']: bestTick=bestState['tick']

    ind = []
    for i in range(n*numSteps):
      ind.append(i)
    
    while True:
      self.shuffle(ind)
      changed=False
      
      for i in ind:
                
        pos=int(i/numSteps)
        if (pos+1 == len(throttles)):
          break
        
        #no point optimizing pieces after crash
        if bestState['crash'] and pos > bestState['finishedN']: continue
        
        change=self.STEP
        
        #change causes throttles to go out of bounds
        if throttles[pos]+change>1 or throttles[pos]+change<0 or throttles[pos+1]-change>1 or throttles[pos+1]-change<0:
          continue

        throttles[pos]+=change
        throttles[pos+1]-=change
        
        endState=self.simulate(throttles, startState, bestTick)        
        score=self.score(endState)
        
        #accept move
        if score<=bestScore:
          if not endState['crash']: bestTick=endState['tick']
          
          if score<bestScore: changed=True
          bestScore=score
          bestState=copy.deepcopy(endState)
        #undo
        else:
          throttles[pos]-=change
          throttles[pos+1]+=change
          
      break
      if not changed: break
      

    return bestState    
    
  
  #round every element in a to some decimals
  def myRound(self, a, decimals):
    for i in range(len(a)): a[i]=round(a[i],2)  
      
  
  def score(self, endState):
    return endState['tick']+1000000*endState['piecesLeft']
    

  def getRandomThrottles(self, n, step):  
    throttles=[]
    numSteps=1/step+1
    
    for i in range(n) :
      val = step * int(random.random()*numSteps)
      throttles.append(val)
      
    return throttles
    
    
  #small throttles that pretty much guarantee that we don't crash
  def getRandomSmallThrottles(self, n, step):  
    throttles=[]
    numSteps=1/step+1
    maxTh=0.3
    
    for i in range(n) :
      while True:
        val = step * int(random.random()*numSteps)
        if val<=maxTh: break
        
      throttles.append(val)
      
    return throttles    
    
  #TODO:
  # - handle the case where you move multiple pieces in one tick, probably rare
  # - compute angle information
  def simulate(self, throttles, startState, maxTicks):
    n=len(throttles)
    
    curState=copy.deepcopy(startState)
    #print 'start',curState
    
    tick=0
    i=0
      
    while i<n and tick<maxTicks:

      tick+=1
      pieceID=curState['piece']
      
      if self.isStraightPiece(pieceID):
        pieceLength=self.getPieceLength(pieceID)
      else:
        pieceLength=self.getCurveLength(pieceID,curState['lane'])
      
      if curState['pieceDistance']+curState['speed'] < pieceLength:
        curState['pieceDistance']+=curState['speed']
        curState['speed']+=self.CONST_T*throttles[i]+self.CONST_V*curState['speed']
      else:
        extraDist=curState['pieceDistance']+curState['speed'] - pieceLength
        curState['pieceDistance']=extraDist
        curState['speed']+=self.CONST_T*throttles[i]+self.CONST_V*curState['speed']
        curState['piece']=self.getNextPieceID(curState['piece'])
        i+=1

      # Update the angle and angular velocity

      alpha  = -self.CONST_A1 * curState['angleVelocity']
      alpha -= self.CONST_A2 * curState['speed'] * curState['angle']
      if self.isCornerPiece(pieceID):
        curveRadius=self.getCurveRadius(pieceID,curState['lane'])
        
        tmp = (curState['speed'] - self.CONST_R1*curveRadius - self.CONST_R2)
        if (tmp > 0):
          if (self.getPieceAngle(pieceID) < 0):
            alpha -= self.CONST_M * tmp
          else:
            alpha += self.CONST_M * tmp

      curState['angleVelocity'] += alpha
      curState['angle'] += curState['angleVelocity']

      #print tick,curState
      
        
      if self.isCornerPiece(pieceID):
        curveRadius=self.getCurveRadius(pieceID,curState['lane'])
        if math.fabs(curState['angle']) > self.MAX_SLIP_ANGLE:
          #print 'CRASH!'
          curState['tick']=tick
          curState['crash']=True
          curState['piecesLeft']=max(1,n-i)      #this includes the current piece, max just in case
          curState['finishedN']=i
          return curState
        
    
    #print "woo!"
    curState['tick']=tick
    curState['crash']=False
    curState['piecesLeft']=n-i
    curState['finishedN']=i
    return curState
    
    
  def shuffle(self, a):
    n=len(a)
    for i in range(n):
      k=int(random.random()*(n-i)+i)
      temp=a[i]
      a[i]=a[k]
      a[k]=temp
    
    
#methods from main.py    
#######################################################################################

  def doesPieceHaveAttribute(self, pieceID, attribute):    
    return (attribute in self.raceData['race']['track']['pieces'][pieceID])
            
  def isCornerPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'angle')
    
  def isStraightPiece(self, pieceID):
    return not(self.isCornerPiece(pieceID))
    
  # Additionally, there may be a switch attribute that indicates that this
  # piece is a switch piece, i.e. you can switch lanes here.      
  def isSwitchPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'switch')      
    
  def getPieceAttribute(self, pieceID, attribute):
    return self.raceData['race']['track']['pieces'][pieceID][attribute]
    
  # NOTE: assume piece is a corner piece  
  # The angle attribute is the angle of the circular segment that the
  # piece forms. For left turns, this is a negative value. For a 45-degree
  # bend to the left, the angle would be -45. For a 30-degree turn to the
  # right, this would be 30.    
  def getPieceAngle(self, pieceID):      
    return self.getPieceAttribute(pieceID, 'angle')

    
  def getNextPieceID(self, pieceID):
    return (pieceID + 1) % self.getNumPieces()
    
  def getNumPieces(self):
    return len(self.raceData['race']['track']['pieces'])      
    
  # For a Bend, there's radius that depicts the radius of the circle
  # that would be formed by this kind of pieces. More specifically,
  # this is the radius at the center line of the track.      
  def getPieceRadius(self, pieceID):
    return self.getPieceAttribute(pieceID, 'radius')
    
  # NOTE: assume lanes are in increasing order of lane index
  # The track may have 1 to 4 lanes. The lanes are described in
  # the protocol as an array of objects that indicate the lane's
  # distance from the track center line. A positive value tells
  # that the lanes is to the right from the center line while a
  # negative value indicates a lane to the left from the center.
  def getLaneDistFromCenter(self, lane):
    return self.raceData['race']['track']['lanes'][lane]['distanceFromCenter']
          
  # NOTE: assume piece is straight
  # For a Straight the length attribute depicts the length of the piece.
  def getPieceLength(self, pieceID):
    return self.getPieceAttribute(pieceID, 'length')
    
    
  # NOTE: assume piece is curved
  def getCurveLength(self, pieceID, lane):
    angle = self.getPieceAngle(pieceID)
    length = self.getCurveRadius(pieceID, lane) * 2 * math.pi * abs(angle) / 360.0
    assert length >= 0
    
    return length

    
  # NOTE: assume piece is curved
  def getCurveRadius(self, pieceID, lane):    
    angle = self.getPieceAngle(pieceID)
    mult = -1
    if angle < 0: mult = +1  # left turn
    radius = self.getPieceRadius(pieceID) + mult * self.getLaneDistFromCenter(lane)
    assert radius >= 0

    return radius
  
    
    
if __name__ == "__main__":
  test=tester()
  test.solve()
  
