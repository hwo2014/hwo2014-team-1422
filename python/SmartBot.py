import json
import socket
import sys
import math
import random
import numpy
import copy
import thread


#TODO
# - compute switch distance, should be around 102, see http://www.reddit.com/r/HWO/comments/23lorl/inpiecedistance_longer_than_piece_length/
# - add boost
# - add switching. At first, switch lanes when there is someone in front of us
# - use fewer throttles before the race finish
# - use multithreading?
# - add computation of R1, R2, A1, A2 and M during qualification period


class SmartBot(object):

### GLOBAL VARIABLES ###

# track selection #
  #TRACK="finland"
  TRACK="germany"    
  #TRACK="usa"
  #TRACK="france"      
  CAR_COUNT = 1
  
  #QUAL: qualification mode, which computes all the physics constants
  #RACE: race mode
  MODE='QUAL'    
  
# physics model #

  STATS=[]
  
  #maxVelocity = const_r1 * radius + const_r2
  CONST_R1=0.0325
  CONST_R2=2.417
  
  #speedNew = speedOld + const_T*throttleOld + const_V*speedOld  
  CONST_T=0.2
  CONST_V=-0.02

  CONST_A1=0.1
  CONST_A2=0.00125
  CONST_M=0.5
  
  MAX_SLIP_ANGLE = 45 

  #optimizer parameters
  STEP = 0.1        #should divide evenly into 1
  LOOK_AHEAD = 10   #how many pieces do we look ahead?
  MIN_ITER = 50     #minimum number of iterations to run
  MAX_ITER = 50     #maximum number of iterations to run  
  

  
# various data and constants #
#TODO: should we make these all capitals to make them stand out?
  gametick = 0
  haveSwitchedThisPiece = 0
  isTurboAvailable = 0
  curThrottle = 0
  prevThrottle = 0				
  previousCarData = {}
  distanceTravelled = 0  
  prevSpeed = 0
  speed = 0
  prevCarAngle = 0  
  angleVelocity = 0
  firstCall = True
  EPS = 1e-9
    
    
  def __init__(self, socket, name, key):
    # set some global variables				
    self.socket = socket
    self.name = name
    self.key = key
    
    self.prevThrottles=[]
    for i in range(self.LOOK_AHEAD): self.prevThrottles.append(self.STEP)          
    

  def msg(self, msg_type, data):
    self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.gametick}))

  def send(self, msg):
    self.socket.send((msg + "\n").encode('utf-8'))

  def join(self):
    if self.TRACK == "finland":
      return self.msg("join", {"name": self.name, "key": self.key})
    else:
      return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": self.TRACK, "carCount": self.CAR_COUNT})

  # example: {"msgType": "throttle", "data": 1.0}
  def throttle(self, throttle):
    self.curThrottle = throttle
    self.changeInThrottle = self.curThrottle-self.prevThrottle
    self.msg("throttle", throttle)


  # Turbo multiplies the power of your car's engine for a short period. The server indicates the availability of turbo using the turboAvailable message.
  #
  # {"msgType": "turboAvailable", "data": {
  #   "turboDurationMilliseconds": 500.0,
  #   "turboDurationTicks": 30,
  #   "turboFactor": 3.0
  # }}
  # You can use your turbo boost any time you like after the server has indicated the availability of it. Just like this.
  # 
  # {"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"}
  # Once you turn it on, you can't turn it off. It'll automatically expire in 30 ticks (or what ever the number was in the turboAvailable message).
  def turbo(self):
    if self.isTurboAvailable == 1:
      self.isTurboAvailable = 0
      print("BOT: Turbo engaged!")
      self.msg("turbo", "Vrrrooooooooom!")
      
  # example: {"msgType": "switchLane", "data": "Left"}    
  def switch(self, mydir):
    if self.haveSwitchedThisPiece == 0:
      self.haveSwitchedThisPiece = 1
      self.msg("switchLane", mydir)
      # print 'switching',mydir

  def ping(self):
    self.msg("ping", {})

  def run(self):
    self.join()
    self.msg_loop()

  def on_join(self, data):
    print("BOT: Joined")
    # print(data)
    self.ping()

  def on_game_start(self, data):
    print("BOT: Race started")
    self.ping()
      
  def on_game_init(self, data):
    print("BOT: in game init")
    print(data)
    self.ping()    
      
    # set some global variables        
    self.raceData = data  # contains all data about the race
      
  def on_crash(self, data):
    print("BOT: Someone crashed")
    self.ping()

  def on_game_end(self, data):
    print("BOT: Race ended")
    self.ping()

  def on_error(self, data):
    print("Error: {0}".format(data))
    self.ping()

  def on_turbo_available(self, data):
    self.isTurboAvailable = 1
    print("Turbo Available!")
    print("    gameTick=" + str(self.gametick) + " duration=" + str(data['turboDurationMilliseconds']) + "ms " + str(data['turboDurationTicks']) + " ticks, factor=" + str(data['turboFactor']))

  def msg_loop(self):
    msg_map = {
        'join': self.on_join,
        'gameStart': self.on_game_start,
        'gameInit': self.on_game_init,
        'carPositions': self.on_car_positions,
        'crash': self.on_crash,
        'gameEnd': self.on_game_end,
        'error': self.on_error,
        'turboAvailable': self.on_turbo_available,
    }
    socket_file = s.makefile()
    line = socket_file.readline()
    while line:
      msg = json.loads(line)
      msg_type, data = msg['msgType'], msg['data']
      if 'gameTick' in msg:
        self.gametick = msg['gameTick']            
      if msg_type in msg_map:
        #print("Got {0}".format(msg_type) + " tick=" + str(self.gametick) + " th=" + str(self.curThrottle) + " d=" + str(self.distanceTravelled) + " sp=" + str(self.prevSpeed))
        msg_map[msg_type](data)
      else:
        print("Got {0}".format(msg_type) + " tick=" + str(self.gametick) + " time=" + str(self.gametick / 60.0))
        self.ping()
      line = socket_file.readline()        
      
### GET METHODS ###
  
  
# raceData #

  def doesPieceHaveAttribute(self, pieceID, attribute):    
    return (attribute in self.raceData['race']['track']['pieces'][pieceID])
            
  def isCornerPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'angle')
    
  def isStraightPiece(self, pieceID):
    return not(self.isCornerPiece(pieceID))
    
  # Additionally, there may be a switch attribute that indicates that this
  # piece is a switch piece, i.e. you can switch lanes here.      
  def isSwitchPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'switch')      
    
  def getPieceAttribute(self, pieceID, attribute):
    return self.raceData['race']['track']['pieces'][pieceID][attribute]
    
  # NOTE: assume piece is a corner piece  
  # The angle attribute is the angle of the circular segment that the
  # piece forms. For left turns, this is a negative value. For a 45-degree
  # bend to the left, the angle would be -45. For a 30-degree turn to the
  # right, this would be 30.    
  def getPieceAngle(self, pieceID):      
    return self.getPieceAttribute(pieceID, 'angle')
              
  def getNextPieceID(self, pieceID):
    return (pieceID + 1) % self.getNumPieces()
    
  def getNumPieces(self):
    return len(self.raceData['race']['track']['pieces'])  
    
  # For a Bend, there's radius that depicts the radius of the circle
  # that would be formed by this kind of pieces. More specifically,
  # this is the radius at the center line of the track.      
  def getPieceRadius(self, pieceID):
    return self.getPieceAttribute(pieceID, 'radius')
    
  # NOTE: assume lanes are in increasing order of lane index
  # The track may have 1 to 4 lanes. The lanes are described in
  # the protocol as an array of objects that indicate the lane's
  # distance from the track center line. A positive value tells
  # that the lanes is to the right from the center line while a
  # negative value indicates a lane to the left from the center.
  def getLaneDistFromCenter(self, lane):
    return self.raceData['race']['track']['lanes'][lane]['distanceFromCenter']
          
  # NOTE: assume piece is straight
  # For a Straight the length attribute depicts the length of the piece.
  def getPieceLength(self, pieceID):
    return self.getPieceAttribute(pieceID, 'length')
    
    
  # NOTE: assume piece is curved
  def getCurveLength(self, pieceID, lane):
    angle = self.getPieceAngle(pieceID)
    length = self.getCurveRadius(pieceID, lane) * 2 * math.pi * abs(angle) / 360.0
    assert length >= 0
    
    return length

    
  # NOTE: assume piece is curved
  def getCurveRadius(self, pieceID, lane):    
    angle = self.getPieceAngle(pieceID)
    mult = -1
    if angle < 0: mult = +1  # left turn
    radius = self.getPieceRadius(pieceID) + mult * self.getLaneDistFromCenter(lane)
    assert radius >= 0

    return radius    
    
# carData #

  # TODO: assumes just 1 player
  # zero based index of the piece the car is on
  def getPieceID(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['pieceIndex']              
    else:
      return data[0]['piecePosition']['pieceIndex']           
          
    
  # TODO: assumes just 1 player      
  # the distance the car's Guide Flag (see above) has travelled from the
  # start of the piece along the current lane    
  # So I guess no need to complex computations for curves?    
  def getInPieceDistance(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['inPieceDistance']          
    else:
      return data[0]['piecePosition']['inPieceDistance']    
    
    
  # TODO: assumes just 1 player     
  # the number of laps the car has completed. The number 0 indicates
  # that the car is on its first lap. The number -1 indicates that it
  # has not yet crossed the start line to begin it's first lap.       
  def getLap(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['lap']      
    else:
      return data[0]['piecePosition']['lap']
             
    
  # TODO: assumes just 1 player, assume startLane==endLane
  # a pair of lane indices. Usually startLaneIndex and endLaneIndex
  # are equal, but they do differ when the car is currently switching lane    
  def getLane(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['lane']['startLaneIndex']      
    else:
      return data[0]['piecePosition']['lane']['startLaneIndex']
    
          
  # TODO: assumes just 1 player  
  # The angle depicts the car's slip angle. Normally this is zero, but
  # when you go to a bend fast enough, the car's tail will start to drift.
  # Naturally, there are limits to how much you can drift without crashing
  # out of the track.            
  def getCarAngle(self, data={}):
    if data == {}:
      return self.carData[0]['angle']                
    else:      
      return data[0]['angle']          
    
    
    
  # distance travelled since last tick
  def getDistanceTravelled(self):  
  
    # start of race, there is no distance travelled yet
    if self.previousCarData == {}: return 0

    curPieceID = self.getPieceID()
    previousPieceID = self.getPieceID(self.previousCarData)
  
    curPieceDistance = self.getInPieceDistance()      
    previousPieceDistance = self.getInPieceDistance(self.previousCarData)
    
  
    previousLane = self.getLane(self.previousCarData)
  
    distToStartOfCurPiece = curPieceDistance      
    distToStartOfPreviousPiece = previousPieceDistance        
  
    if self.isCornerPiece(previousPieceID):    
      prevPieceLength = self.getCurveLength(previousPieceID, previousLane)    
    else:
      prevPieceLength = self.getPieceLength(previousPieceID)
    
    distToEndOfPreviousPiece = prevPieceLength - distToStartOfPreviousPiece        
    #IF THIS ASSERT FAILS then there is something wrong... please record where it fails
    # fails after pre-corner pieceID = 17 on USA (lap=0 tick=282 d=1660.6917330181439) 
    # note server reported piece distance measurements are all based on the centre line-length
    # using lane-length calculations might be causing error.
    assert distToEndOfPreviousPiece >= 0
  
    # if on the same piece
    if curPieceID == previousPieceID:
      assert distToStartOfCurPiece - distToStartOfPreviousPiece >= 0
      return distToStartOfCurPiece - distToStartOfPreviousPiece
     
    # on a new track piece
    self.haveSwitchedThisPiece = 0
    return distToStartOfCurPiece + distToEndOfPreviousPiece
    

### AI METHODS ###

  def on_car_positions(self, data):
  
    self.carData = data  
    # print(data)
  
    self.updateBeforeThrottle()
    if self.MODE=='QUAL':
      self.doQualification()
    elif self.MODE=='RACE':      
      #self.AI_fixed()
      self.AI_simulator3()
    self.updateAfterThrottle()
    
    
   
     
     
#############################################################################
    
    
  def doQualification(self):
    
    pieceID = self.getPieceID()    

    #set a small constant throttle    
    self.throttle(0.2)                
            
    if self.isStraightPiece(pieceID) and abs(self.getCarAngle()) < self.EPS and self.prevSpeed > self.EPS:
      self.STATS.append({'s':self.prevSpeed, 'a':self.acceleration, 't':self.prevThrottle})      
            
    if len(self.STATS)==2:    
      print self.STATS
      st1=self.STATS[0]
      st2=self.STATS[1]            
      
      #using numpy
      a = numpy.array([[st1['t'],st1['s']],[st2['t'],st2['s']]])
      b = numpy.array([st1['a'],st2['a']])
      x = numpy.linalg.solve(a, b)            
      self.CONST_T=x[0]
      self.CONST_V=x[1]
      
      #using a toy matrix library
      #a = matrix.matrix([[st1['t'],st1['s']],[st2['t'],st2['s']]])
      #b = matrix.vector([st1['a'],st2['a']])
      #x = matrix.solve(a,b)
      #self.CONST_T=x.get((0,0))
      #self.CONST_V=x.get((1,0))      
      
      #using my brain :)
      #self.CONST_V=(st2['a']*st1['t']-st1['a']*st2['t'])/(st2['s']*st1['t']-st1['s']*st2['t'])
      #self.CONST_T=(st1['a']-self.CONST_B*st1['s'])/st1['t']      
      print("tick=" + str(self.gametick) + ", speedNew = speedOld + " + str(self.CONST_T)+"*throttleOld + " + str(self.CONST_V)+"*speedOld")
      self.MODE='RACE'
      
    
    
    
  def updateBeforeThrottle(self):
    self.dist = self.getDistanceTravelled()  # distance travelled since last tick
    self.distanceTravelled += self.dist      # total distance travelled
    self.speed = self.dist / 1               # instantaneous speed. NOTE: assume we didn't miss any ticks
    self.acceleration = (self.speed - self.prevSpeed) / 1
    # average speed    
    if self.gametick == 0:
      self.averageSpeed = 0        
    else:
      self.averageSpeed = self.distanceTravelled / self.gametick  
      
      
    #print 'tick',self.gametick,'dist',self.dist,'speed',self.speed,'accel',self.acceleration

    pieceID = self.getPieceID()    
    
    # find relationship between throttle, speed and acceleration on straight pieces  
    #if self.isStraightPiece(pieceID) and abs(self.getCarAngle()) < self.EPS:
      #print self.prevSpeed, self.acceleration
      
    previousPieceID = self.getPieceID(self.previousCarData)
    previousLane = self.getLane(self.previousCarData)     
    self.carAngle=self.getCarAngle()
    self.changeInAngle=self.carAngle-self.prevCarAngle    
  
    if self.isCornerPiece(previousPieceID):      
      prevRadius = self.getCurveRadius(previousPieceID, previousLane) 
      #print self.prevCarAngle, self.changeInAngle, prevRadius
      
      
    # Update the angle and angular velocity
    alpha  = -self.CONST_A1 * self.angleVelocity
    alpha -= self.CONST_A2 * self.speed * self.carAngle
    if self.isCornerPiece(pieceID):
      curveRadius=self.getCurveRadius(pieceID,self.getLane())
            
      tmp = (self.speed - self.CONST_R1*curveRadius - self.CONST_R2)
      if (tmp > 0):
        if (self.getPieceAngle(pieceID) < 0):
          alpha -= self.CONST_M * tmp
        else:
          alpha += self.CONST_M * tmp

    self.angleVelocity += alpha
    
    
  def updateAfterThrottle(self):
    self.previousCarData = self.carData      
    self.prevSpeed = self.speed
    self.prevCarAngle = self.carAngle
    self.prevThrottle = self.curThrottle

    
    
  #drive at a constant speed
  #constant speed occurs when T*throttle+V*speed=0
  def AI_constantSpeed(self):
    speed=5
    th=-self.CONST_V*speed/self.CONST_T
    self.throttle(th)
      
    
  def AI_basic(self):      
    th = 0.6
    self.throttle(th)      
      
      
      
  def AI_fixed(self):
  
    if self.TRACK=='finland':
      #1480, assumes maxSlip=60, fastest lap 8.07
      th=[0.9, 1.0, 1.0, 1.0, 0.2, 0.8, 0.6, 0.6, 1.0, 0.5, 0.1, 0.7, 1.0, 0.5, 0.8, 0.3, 0.5, 1.0, 1.0, 0.2, 1.0, 0.5, 1.0, 1.0, 0.5, 1.0, 0.2, 0.7, 0.7, 0.3, 0.9, 0.4, 1.0, 0.5, 1.0, 1.0,1.0, 0.9, 1.0, 1.0,0.3, 1.0, 0.9, 0.9, 0.0, 0.4, 1.0, 0.9, 0.9, 1.0, 1.0, 0.7, 1.0, 0.0, 0.6, 0.0, 1.0, 0.7, 0.6, 0.9, 0.6, 0.7, 1.0, 0.8, 0.7, 0.9, 0.2, 0.8, 0.2, 0.9, 0.3, 0.9, 0.5, 0.9, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9,1.0, 1.0, 0.9, 0.3, 0.0, 1.0, 0.5, 1.0, 1.0, 1.0, 0.8, 1.0, 0.4, 0.4, 0.6, 0.3, 0.6, 1.0, 0.9, 0.5, 0.7, 0.7, 0.9, 1.0, 1.0, 0.8, 0.3, 0.3, 1.0, 0.2, 1.0, 1.0, 0.3, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
      #1453, assume maxSlip=60, crashes :(
      #th=[1.0, 1.0, 1.0, 1.0, 0.3, 0.4, 1.0, 0.8, 0.9, 1.0, 0.8, 0.9, 1.0, 0.4, 0.2, 0.1, 1.0, 0.8, 0.5, 0.5, 0.9, 0.8, 0.7, 1.0, 0.8, 0.7, 0.4, 0.6, 0.6, 0.5, 0.7, 0.9, 0.3, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.1, 0.4, 0.8, 0.5, 0.8, 0.9, 1.0, 1.0, 0.9, 1.0, 1.0, 0.1, 0.3, 0.1, 1.0, 0.7, 0.7, 0.9, 0.5, 0.6, 1.0, 0.9, 1.0, 0.9, 0.2, 0.4, 1.0, 0.2, 0.8, 0.9, 0.3, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.8, 1.0, 0.8, 0.5, 0.6, 0.0, 1.0, 0.7, 0.9, 1.0, 1.0, 1.0, 0.8, 0.7, 0.6, 0.0, 0.2, 1.0, 1.0, 0.9, 0.3, 0.7, 0.7, 1.0, 1.0, 1.0, 0.6, 0.3, 0.9, 0.5, 0.4, 0.6, 1.0, 0.4, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
      
      
    if self.TRACK=='germany':
      #1731, assumes maxSlip=60, crashes :(      
      #1998, assumes maxSlip=30, fastest lap 10.87
      th=[0.5, 0.7, 0.4, 0.5, 0.8, 0.2, 0.0, 0.2, 0.1, 0.4, 1.0, 0.6, 1.0, 1.0, 0.2, 0.7, 0.3, 0.5, 0.3, 0.9, 1.0, 0.9, 0.1, 0.1, 0.3, 0.7, 0.8, 0.2, 0.5, 0.7, 1.0, 1.0, 0.2, 0.9, 0.6, 0.8, 0.1, 0.8, 0.3, 1.0, 1.0, 0.7, 0.7, 0.4, 0.4, 0.1, 0.6, 0.4, 0.9, 1.0, 0.8, 0.4, 0.6, 1.0, 0.9, 1.0, 1.0,0.4, 0.0, 0.0, 0.7, 0.2, 0.4, 0.1, 0.6, 0.3, 0.2, 0.4, 1.0, 0.6, 0.8, 0.9, 0.2, 0.9, 0.6, 0.2, 0.5, 1.0, 0.4, 0.2, 0.1, 0.7, 1.0, 0.6, 0.1, 0.7, 1.0, 1.0, 1.0, 1.0, 0.3, 0.4, 0.9, 0.0, 0.5, 0.9, 0.8, 1.0, 0.2, 0.8, 0.6, 0.5, 0.1, 0.5, 0.1, 0.5, 0.5, 1.0, 0.9, 0.7, 0.2, 0.6, 0.9, 0.4,0.8, 0.3, 0.1, 0.3, 0.4, 0.3, 0.3, 0.6, 0.3, 0.2, 0.9, 0.5, 0.9, 0.7, 0.9, 0.1, 1.0, 0.3, 0.3, 0.9, 1.0, 0.7, 0.0, 0.5, 0.3, 0.8, 1.0, 0.0, 0.8, 1.0, 1.0, 1.0, 0.8, 0.7, 0.6, 0.5, 0.1, 0.3, 1.0, 0.9, 1.0, 0.8, 0.7, 0.4, 0.4, 0.1, 0.2, 1.0, 0.5, 1.0, 1.0, 0.3, 0.8, 1.0, 1.0, 1.0, 1.0]
      
    if self.TRACK=='usa':      
      #945, assumes maxSlip=60, fastest lap 5.2
      th=[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
      
      
    if self.TRACK=='france':
      #1401, assumes maxSlip=60, crashes :(
      #1567, assumes maxSlip=30, fastest lap 8.58
      th=[1.0, 0.8, 1.0, 0.7, 0.9, 0.6, 0.3, 0.8, 0.4, 0.2, 1.0, 0.5, 0.4, 0.3, 0.2, 0.4, 0.5, 1.0, 1.0, 0.1, 0.3, 0.0, 1.0, 0.4, 1.0, 0.2, 0.8, 0.5, 0.8, 1.0, 1.0, 0.6, 0.2, 0.5, 0.9, 0.7, 1.0, 1.0, 0.9, 0.9, 1.0, 1.0, 0.2, 1.0,1.0, 1.0, 1.0, 0.8, 0.4, 0.3, 0.3, 1.0, 0.7, 0.3, 1.0, 1.0, 0.0, 0.0, 0.5, 0.7, 0.4, 1.0, 0.2, 0.3, 0.5, 0.7, 0.6, 0.2, 0.9, 0.8, 0.2, 0.6, 0.7, 1.0, 0.9, 1.0, 0.5, 1.0, 0.3, 0.6, 0.7, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,0.6, 0.9, 0.7, 0.7, 0.8, 0.1, 0.5, 0.8, 0.8, 0.8, 1.0, 0.5, 0.4, 0.0, 0.1, 0.6, 0.4, 1.0, 0.5, 0.2, 0.5, 0.2, 0.9, 0.9, 0.8, 0.6, 0.2, 0.7, 0.9, 0.9, 1.0, 1.0, 0.1, 1.0, 0.3, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
      

        
    pieceID = self.getPieceID() + self.getLap()*self.getNumPieces()
    
    if pieceID < len(th):
      self.throttle(th[pieceID])
    else:
      self.throttle(1.0)
      
      

############## Simulator ########################

  # solve for every tick
  # results on finland
  # lookAhead=5, step=0.1, minIter=1, maxIter=10, maxSlip=30, fastest lap 8.33  
  # lookAhead=10, step=0.5, minIter=1, maxIter=10, maxSlip=30, fastest lap 8.33
  # lookAhead=10, step=0.1, minIter=1, maxSlip=30, fastest lap 8.55
  # lookAhead=20, step=0.5, minIter=1, maxIter=10, maxSlip=30, fastest lap 8.65  
  # lookAhead=10, step=1, minIter=1, maxIter=10, maxSlip=30, crash
  # lookAhead=5, step=0.5, minIter=1, maxIter=10, maxSlip=30, crash  
  def AI_simulator(self):
        
    curPieceID = self.getPieceID()
    previousPieceID = self.getPieceID(self.previousCarData)   
    
      
    #reuse previous throttles
    if curPieceID==previousPieceID:
      throttles=self.prevThrottles
    else:
      throttles=self.prevThrottles[1:]
      throttles.append(self.STEP)

      
    throttles=self.solve(throttles)
    self.prevThrottles = throttles


    self.throttle(throttles[0])      
    
    
  # only solve when it is your first time on a piece
  # results on finland  
  # lookAhead=10, step=0.1, minIter=50, maxIter=50, maxSlip=30, fastest lap 8.12        
  # lookAhead=10, step=0.05, minIter=50, maxIter=50, maxSlip=30, fastest lap 8.15
  # lookAhead=20, step=0.5, minIter=100, maxIter=100, maxSlip=30, fastest lap 8.43  
  # lookAhead=10, step=0.5, minIter=50, maxIter=50, maxSlip=30, fastest lap 8.50  
  # lookAhead=20, step=0.5, minIter=1, maxIter=10, maxSlip=30, fastest lap 9.27  
  # lookAhead=10, step=0.01, minIter=50, maxIter=50, maxSlip=30, time out  
  def AI_simulator2(self):
  
        
    curPieceID = self.getPieceID()
    previousPieceID = self.getPieceID(self.previousCarData) 
    
      
    #reuse previous throttles
    if curPieceID!=previousPieceID or self.firstCall:
      throttles=self.prevThrottles[1:]
      throttles.append(self.STEP)
      throttles=self.solve(throttles)          
    else:
      throttles=self.prevThrottles


    self.firstCall=False           
    self.prevThrottles = throttles    
    
    self.throttle(throttles[0])      

    
    
  # solve every time you are on a new piece or if enough ticks occurred
  # lookAhead=10, step=0.1, minIter=50, maxIter=50, maxTicks=15, maxSlip=60, fastest lap 7.65 + crash ***
  # lookAhead=10, step=0.1, minIter=50, maxIter=50, maxTicks=15, maxSlip=45, fastest lap 7.98 + crash
  # lookAhead=10, step=0.1, minIter=50, maxIter=50, maxTicks=15, maxSlip=30, fastest lap 8.13
  # lookAhead=10, step=0.1, minIter=25, maxIter=25, maxTicks=15, maxSlip=30, fastest lap 8.33
  # lookAhead=15, step=0.1, minIter=50, maxIter=50, maxTicks=15, maxSlip=45, fastest lap 8.52 + crash    
  def AI_simulator3(self):
        
    maxTicks=15
    crashLookAhead=3    #how many pieces to check for crashes?
        
    curPieceID = self.getPieceID()
    previousPieceID = self.getPieceID(self.previousCarData) 
    
      
    #reuse previous throttles
    if curPieceID!=previousPieceID or self.firstCall:
      throttles=self.prevThrottles[1:]
      throttles.append(self.STEP)
      self.gametickLastSolve=self.gametick
      throttles=self.solve(throttles)    
    elif self.gametick-self.gametickLastSolve >= maxTicks:
      throttles=self.prevThrottles    
      self.gametickLastSolve=self.gametick      
      throttles=self.solve(throttles)          
    else:
      throttles=self.prevThrottles


    self.firstCall=False           
    self.prevThrottles = throttles    
    
    
    startState={'piece':self.getPieceID(),
                'pieceDistance':self.getInPieceDistance(),
                'speed':self.speed,
                'angle':self.getCarAngle(),
                'lane':self.getLane(),
                'angleVelocity':self.angleVelocity}    
                
    bestTick=10000

    #simulate a few pieces to see if we will crash
    endState=self.simulate(throttles[0:crashLookAhead], startState, bestTick)  
    
    if endState['crash']:
      print 'tick',self.gametick,'looks like we will crash...'
      self.throttle(0.0)
    else:
      self.throttle(throttles[0])      
     

        
        


  def solve(self, throttles):
    #print(self.raceData)
    
    doPrint=True
    minIter = self.MIN_ITER           #minimum number of iterations to run
    maxIter = self.MAX_ITER           #maximum number of iterations to run
    n = self.LOOK_AHEAD
    step = self.STEP
    bestTick=10000
    bestScore=1e20
    loadBest=True
    maxMutations=0
    
    #0 - no optimization, just rely on random mutations
    #1 - change the value of one throttle
    #2 - increase one throttle, while decreasing its neighbour. On its own this seems quite weak
    #3 - do 1 and 2
    optimizeType='1'
    
    
    startState={'piece':self.getPieceID(),
                'pieceDistance':self.getInPieceDistance(),
                'speed':self.speed,
                'angle':self.getCarAngle(),
                'lane':self.getLane(),
                'angleVelocity':self.angleVelocity}
    
    
    bestThrottles=copy.deepcopy(throttles)

    endState=self.simulate(throttles, startState, bestTick)        
    bestScore=self.score(endState)
    willCrash=True

    numSteps=1/step+1    
    
    myiter=0
    while True:
      
      #load the current best solution
      if loadBest: throttles=copy.deepcopy(bestThrottles)       
               
      #perform some mutations
      if myiter>0:
        mutations=int(random.random()*maxMutations+1)
        for i in range(mutations):
          pos = int(random.random()*n)
          throttles[pos] = step * int(random.random()*numSteps)
      
      
      #possibly perform some optimization on throttles
      if optimizeType=='0':
        endState=self.simulate(throttles, startState, bestTick)    
      if optimizeType=='1' or optimizeType=='3':
        endState=self.optimize1(startState, throttles, n, step)
      if optimizeType=='2' or optimizeType=='3':
        endState=self.optimize2(startState, throttles, n, step)
        
      
      
      score=self.score(endState)
      
      if score<=bestScore:
        if not(endState['crash']):
          bestTick=endState['tick']
          willCrash=False
        
        bestThrottles=copy.deepcopy(throttles)
        
        self.myRound(throttles,2)   #round for nicer output
        
        if score<bestScore and doPrint:
          print 'tick',self.gametick,'iter',myiter,'score',score,throttles
            
        bestScore=score        
        
      myiter+=1
      
      #termination
      if myiter>=minIter and not(willCrash): break
      if myiter>=maxIter: break
        
        
    return bestThrottles
    
  
  # a move consists of changing the value of one throttle
  def optimize1(self, startState, throttles, n, step):
  
    bestTick=10000
    numSteps=int(1/step)+1       
    
    bestState=self.simulate(throttles, startState, bestTick)
    bestScore=self.score(bestState)    
    if not bestState['crash']: bestTick=bestState['tick']

    ind = []
    for i in range(n*numSteps):
      ind.append(i)
    
    while True:
      self.shuffle(ind)
      changed=False
      
      for i in ind:
        pos=int(i/numSteps)
        
        #no point optimizing pieces after crash
        if bestState['crash'] and pos > bestState['finishedN']: continue
        
        val=(i%numSteps)*step
        
        if throttles[pos]==val: continue    #same value
        
        #no point decreasing throttle if we are not crashing
        if not(bestState['crash']) and val<=throttles[pos] - self.STEP:
          continue
                  
        #TODO: no point increasing throttle in pieces before the crash?

        oldVal=throttles[pos]
        throttles[pos]=val
        
        endState=self.simulate(throttles, startState, bestTick)        
        score=self.score(endState)
        
        #accept move
        if score<=bestScore:
          if not endState['crash']: bestTick=endState['tick']
          
          if score<bestScore: changed=True
          bestScore=score
          bestState=copy.deepcopy(endState)
        #undo
        else:
          throttles[pos]=oldVal
          
      break
      #if not changed: break
      

    return bestState
    
    
    
  # a move consists of changing increasing one throttle, while decreasing its neighbour
  def optimize2(self, startState, throttles, n, step):
  
    bestTick=10000
    numSteps=int(1/step)+1       
    
    bestState=self.simulate(throttles, startState, bestTick)
    bestScore=self.score(bestState)    
    if not bestState['crash']: bestTick=bestState['tick']

    ind = []
    for i in range(n*numSteps):
      ind.append(i)
    
    while True:
      self.shuffle(ind)
      changed=False
      
      for i in ind:
                
        pos=int(i/numSteps)
        if (pos+1 == len(throttles)):
          break
        
        #no point optimizing pieces after crash
        if bestState['crash'] and pos > bestState['finishedN']: continue
        
        change=self.STEP
        
        #change causes throttles to go out of bounds
        if throttles[pos]+change>1 or throttles[pos]+change<0 or throttles[pos+1]-change>1 or throttles[pos+1]-change<0:
          continue

        throttles[pos]+=change
        throttles[pos+1]-=change
        
        endState=self.simulate(throttles, startState, bestTick)        
        score=self.score(endState)
        
        #accept move
        if score<=bestScore:
          if not endState['crash']: bestTick=endState['tick']
          
          if score<bestScore: changed=True
          bestScore=score
          bestState=copy.deepcopy(endState)
        #undo
        else:
          throttles[pos]-=change
          throttles[pos+1]+=change
          
      break
      #if not changed: break
      

    return bestState    
    
  
  #round every element in a to some decimals
  def myRound(self, a, decimals):
    for i in range(len(a)): a[i]=round(a[i],2)  
      
  
  def score(self, endState):
    return endState['tick']+1000000*endState['piecesLeft']
    
    
  
    
  #TODO:
  # - handle the case where you move multiple pieces in one tick, probably rare
  # - compute angle information
  def simulate(self, throttles, startState, maxTicks):
    n=len(throttles)
    
    curState=copy.deepcopy(startState)
    #print 'start',curState
    
    tick=0
    i=0
      
    while i<n and tick<maxTicks:

      tick+=1
      pieceID=curState['piece']
      
      if self.isStraightPiece(pieceID):
        pieceLength=self.getPieceLength(pieceID)
      else:
        pieceLength=self.getCurveLength(pieceID,curState['lane'])
      
      if curState['pieceDistance']+curState['speed'] < pieceLength:
        curState['pieceDistance']+=curState['speed']
        curState['speed']+=self.CONST_T*throttles[i]+self.CONST_V*curState['speed']
      else:
        extraDist=curState['pieceDistance']+curState['speed'] - pieceLength
        curState['pieceDistance']=extraDist
        curState['speed']+=self.CONST_T*throttles[i]+self.CONST_V*curState['speed']
        curState['piece']=self.getNextPieceID(curState['piece'])
        i+=1

      # Update the angle and angular velocity

      alpha  = -self.CONST_A1 * curState['angleVelocity']
      alpha -= self.CONST_A2 * curState['speed'] * curState['angle']
      if self.isCornerPiece(pieceID):
        curveRadius=self.getCurveRadius(pieceID,curState['lane'])
        
        tmp = (curState['speed'] - self.CONST_R1*curveRadius - self.CONST_R2)
        if (tmp > 0):
          if (self.getPieceAngle(pieceID) < 0):
            alpha -= self.CONST_M * tmp
          else:
            alpha += self.CONST_M * tmp

      curState['angleVelocity'] += alpha
      curState['angle'] += curState['angleVelocity']

      #print tick,curState
      
        
      if self.isCornerPiece(pieceID):
        curveRadius=self.getCurveRadius(pieceID,curState['lane'])
        if math.fabs(curState['angle']) > self.MAX_SLIP_ANGLE:
          #print 'CRASH!'
          curState['tick']=tick
          curState['crash']=True
          curState['piecesLeft']=max(1,n-i)      #this includes the current piece, max just in case
          curState['finishedN']=i
          return curState
        
    
    #print "woo!"
    curState['tick']=tick
    curState['crash']=False
    curState['piecesLeft']=n-i
    curState['finishedN']=i
    return curState
    
    
  def shuffle(self, a):
    n=len(a)
    for i in range(n):
      k=int(random.random()*(n-i)+i)
      temp=a[i]
      a[i]=a[k]
      a[k]=temp      
        


if __name__ == "__main__":
  if len(sys.argv) != 5:
    print("Usage: ./run host port botname botkey")
  else:
    host, port, name, key = sys.argv[1:5]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = SmartBot(s, name, key)
    bot.run()
