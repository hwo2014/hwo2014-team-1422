import json
import socket
import sys
import math
import random
import copy
import time


class SpeedBot1(object):

### GLOBAL VARIABLES ###

  ##IMPORTANT VARIABLE!!!
  #CONFIG="final"    #for final submission to the server
  CONFIG="test"     #for testing  

# track selection #
  TRACK="keimola" # "finland"
  #TRACK="germany"    
  #TRACK="usa"
  #TRACK="france"      
  CAR_COUNT = 1
  
  #QUAL: qualification mode, which computes all the physics constants
  #RACE: race mode
  MODE='QUAL'    
  
# physics model #

  STATS=[]    #for speed model
    
  
  #speedNew = speedOld + const_T*throttleOld + const_V*speedOld  
  CONST_T=0.2
  CONST_V=-0.02
  
  #1 uses R1, R2 and M
  #2 uses B1 and B2. DOESN'T WORK!!! DONT USE!!!
  #3 from http://www.reddit.com/r/HWO/comments/245yms/how_to_calculate_speed_at_which_you_start_slipping/
  #   this uses Fslip and DANGER_FACTOR. Doesn't use MAX_SLIP_ANGLE
  SLIP_MODEL=3

  CONST_A1=0.1
  CONST_A2=0.00125
  
  CONST_R1=0.0325
  CONST_R2=2.417
  CONST_M=0.5
  
  CONST_B1=0.4
  CONST_B2=2.8  
  
  CONST_QUALIFY_THROTTLE = 0.9
  CONST_CORNER_SPEED_LIMIT = 6.2
  CONST_CORNER_THROTTLE_REDUCE = 0.9
  
  Fslip=-1
  foundCONST=False
    
  #how much extra speed (in %) are we willing to take in corners. 1 is no danger
  #1.4 crashes on finland
  #1.2 crashes on germany
  #1.2 works on finland
  #1.1 crashed on CI qualification
  DANGER_FACTOR=1.0

#   speedThresholdFactor = 0.8 # 8.80 lap 3 keimola
#   speedThresholdFactor = 0.9 # 7.617 lap 3 keimola
#   speedThresholdFactor = 0.95 # crash lap 1 keimola
#   speedThresholdFactor = 0.92 # 14.52 crash lap 1 keimola
#   speedThresholdFactor = 0.91 # 7.53 lap 3 keimola
#   speedThresholdFactor = 0.91 # 7.90 lap 2 keimola switching outside a
#   speedThresholdFactor = 0.915 # crash keimola switching outside b
#   speedThresholdFactor = 0.914 # crash keimola switching outside b
#   speedThresholdFactor = 0.913 # crash keimola switching outside b
#   speedThresholdFactor = 0.912 # crash lap1&2, 8.02 lap 3 keimola switching outside b
#   speedThresholdFactor = 0.911 # 8.067 keimola switching outside b
#   speedThresholdFactor = 0.912 # 8.05 keimola switching outside b
#   speedThresholdFactor = 0.9422 # ??? keimola switching outside b
#   speedThresholdFactor = 0.9424 # ??? keimola switching outside b
#   speedThresholdFactor = 0.9425 # ??? keimola switching outside b
#   speedThresholdFactor = 0.9426 # ??? keimola switching outside b
#   speedThresholdFactor = 0.943 # ??? keimola switching outside b
  speedThresholdFactor = 1.0
  
  MAX_SLIP_ANGLE = 60 
  
  NO_TURBO = True     #if true then set turbo off!
  
  #max allowed time since carPosition message in seconds
  MAX_TIME=1.0

  #optimizer parameters
  STEP = 0.05        #should divide evenly into 1
  LOOK_AHEAD = 9   #how many pieces do we look ahead?
  
  
  SWITCH_PROBABILITY = 0.01
  
  DEFAULT_SPEED_LIMIT = 5.0
  
  EPS = 1e-9    
  
  gametick=0
  

# various data and constants #  
    
  def __init__(self, socket, name, key):
    # set some global variables				
    self.socket = socket
    self.name = name
    self.key = key
    
    self.prevCommands={'th':[],'turbo':False}
    # Maps previous throttle used per pieceID. Speeds up solver after 1st lap.
    self.prevLapPieceSpeedLimit = []
    self.prevLapPieceLane = []
    
    # Save speed per position in a csv file:
    self.speedFile = open("stats/" + self.TRACK + "SpeedPlot.csv", "w", encoding="utf-8")
    print("distance,speed,throttle,angle", file=self.speedFile)
    
  def getFastLane(self, lap, piece):
    fastLane = self.prevLapPieceLane[lap][piece]
    return fastLane

  def setFastLane(self, lap, piece, lane):
    self.prevLapPieceLane[lap][piece] = lane

  def getSpeedLimit(self, lap, piece, inPieceDistance):
#     print("get speed limit", lap, piece, round(inPieceDistance, 1))
    numLaps = self.getNumLaps()
    numPieces = self.getNumPieces()
    if lap < 0 or piece >= numPieces or (lap == 0 and piece == 0):
      return 10.0
    if lap >= numLaps:
      return 0.0
    speedLimit = self.prevLapPieceSpeedLimit[lap][piece][int(inPieceDistance)]
    return speedLimit
#     if speedLimit == self.DEFAULT_SPEED_LIMIT:
#       # Don't use the default limit
#       p = piece
#       d = int(inPieceDistance + 1)
#       length = len(self.prevLapPieceSpeedLimit[0][p])
#       while d < length and speedLimit == self.DEFAULT_SPEED_LIMIT:
# #         print("a", lap, p, d)
#         speedLimit = self.prevLapPieceSpeedLimit[lap][p][d]
#         d += 1
#       
#       if speedLimit == self.DEFAULT_SPEED_LIMIT:
#         p = self.getNextPieceID(p)
#         while p < numPieces and speedLimit == self.DEFAULT_SPEED_LIMIT:
#           d = 0
# #           print("a", lap, p, "numPieces", numPieces)
#           length = len(self.prevLapPieceSpeedLimit[0][p])
#           while d < length and speedLimit == self.DEFAULT_SPEED_LIMIT:
# #             print("b", lap, p, d)
#             speedLimit = self.prevLapPieceSpeedLimit[lap][p][d]
#             d += 1
#           p += 1
#         if speedLimit == self.DEFAULT_SPEED_LIMIT:
#           l = lap + 1
#           while l < numLaps and speedLimit == self.DEFAULT_SPEED_LIMIT:
#             p = 0
#             while p < numPieces and speedLimit == self.DEFAULT_SPEED_LIMIT:
#               d = 0
#               length = len(self.prevLapPieceSpeedLimit[0][p])
#               while d < length and speedLimit == self.DEFAULT_SPEED_LIMIT:
# #                 print("c", l, p, d)
#                 speedLimit = self.prevLapPieceSpeedLimit[l][p][d]
#                 d += 1
#               p += 1
#             l = l + 1
#           
# #     print("get speed limit", lap, piece, round(inPieceDistance, 1), speedLimit)
#     return speedLimit
 
  def setSpeedLimit(self, lap, piece, inPieceDistance, speedLimit):
#     print("set speed limit", lap, piece, round(inPieceDistance, 1), speedLimit)
    if lap < 0 or lap >= self.getNumLaps(): return
    self.prevLapPieceSpeedLimit[lap][piece][int(inPieceDistance)] = speedLimit
  
  def setDefaultSpeedLimitsFromReplayFile(self, file):
    print("setting default speed limits based on track in replay file " + str(file))
    line = file.readline()
    while line:
      messages = json.loads(line)
      pieceIndex = 0
      inPieceDistance = 0
      lane = 0
      lap = 0
      for msg in messages:
        msgType = msg['msgType']
#         print("load msg type=" + msgType)
        if msgType == "gameInit":
          self.raceData = msg['data']
          laps = self.getNumLaps()
          pieces = self.getNumPieces()
          print("gameInit", "laps", laps, "pieces", pieces)
          for lap in range(laps):
            self.prevLapPieceSpeedLimit.append([])
            self.prevLapPieceLane.append([])
            for piece in range(pieces):
              self.prevLapPieceSpeedLimit[lap].append([])
              self.prevLapPieceLane[lap].append(0)
              isCorner = self.isCornerPiece(piece) and self.getPieceRadius(piece) < 150
              for inPieceDistance in range(int(self.getPieceLength(piece) + math.pi * self.getNumLanes() * abs(self.getLaneDistFromCenter(0)))):
                if isCorner:
                  self.prevLapPieceSpeedLimit[lap][piece].append(self.CONST_CORNER_SPEED_LIMIT)
                else:
                  self.prevLapPieceSpeedLimit[lap][piece].append(self.DEFAULT_SPEED_LIMIT)
          return
          
      line = file.readline()

  def setSpeedLimitsFromReplayFile(self, file):
    print("reading race data from file " + str(file))
    line = file.readline()
    while line:
      messages = json.loads(line)
      pieceIndex = 0
      inPieceDistance = 0
      lap = 0
      for msg in messages:
        msgType = msg['msgType']
#         print("load msg type=" + msgType)
        if msgType == "gameInit":
          self.raceData = msg['data']
          laps = self.getNumLaps()
          pieces = self.getNumPieces()
          print("gameInit", "laps", laps, "pieces", pieces)
          for lap in range(laps):
            self.prevLapPieceSpeedLimit.append([])
            self.prevLapPieceLane.append([])
            for piece in range(pieces):
              self.prevLapPieceSpeedLimit[lap].append([])
              self.prevLapPieceLane[lap].append(0)
              isCorner = self.isCornerPiece(piece) and self.getPieceRadius(piece) < 150
              for inPieceDistance in range(int(self.getPieceLength(piece) + math.pi * self.getNumLanes() * abs(self.getLaneDistFromCenter(0)))):
                if isCorner:
                  self.prevLapPieceSpeedLimit[lap][piece].append(self.CONST_CORNER_SPEED_LIMIT)
                else:
                  self.prevLapPieceSpeedLimit[lap][piece].append(self.DEFAULT_SPEED_LIMIT)

        if msgType == "fullCarPositions":
          lap = msg['data'][0]['piecePosition']['lap']
            
          pieceIndex = msg['data'][0]['piecePosition']['pieceIndex']    
          inPieceDistance = msg['data'][0]['piecePosition']['inPieceDistance']
          lane = msg['data'][0]['piecePosition']['lane']['endLaneIndex']
          if inPieceDistance > 0.75 * self.getPieceLength(pieceIndex, lane):
            self.setFastLane(lap, pieceIndex, lane)
          
        if msgType == "carVelocities" and self.isStraightPiece(pieceIndex):
          velocityX = msg['data'][0]['x']     
          velocityY = msg['data'][0]['y']
          speed = math.sqrt(velocityX * velocityX + velocityY * velocityY)
#           throttle = (speed - prevSpeed + 0.01995*prevSpeed)/0.2095
#           prevSpeed = speed
          prevMax = self.getSpeedLimit(lap, pieceIndex, inPieceDistance)
          if speed > prevMax:
            newMax = speed
          else:
            newMax = prevMax
          self.setSpeedLimit(lap, pieceIndex, inPieceDistance, newMax)
          
      line = file.readline()

  def overrideSpeedLimits(self):
    # start of race:
    self.prevLapPieceSpeedLimit[0][0][0] = 10.0
    
    
    # BOT: We crashed l: 0 p: 31 d: 10 sp: 6.95 th 0.0
#    self.prevLapPieceSpeedLimit[0][31][10] = 7.1
    # BOT: We crashed l: 0 p: 31 d: 18 sp: 6.98 th 0.0
    for d in range(0, int(len(self.prevLapPieceSpeedLimit[0][30]))):
      if self.prevLapPieceSpeedLimit[0][30][d] > 6.3 or self.prevLapPieceSpeedLimit[0][30][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[0][30][d] = 6.3
    self.prevLapPieceSpeedLimit[0][31][15] = 6.3
    self.prevLapPieceSpeedLimit[0][31][16] = 6.3
    self.prevLapPieceSpeedLimit[0][31][17] = 6.3
    self.prevLapPieceSpeedLimit[0][31][18] = 6.3
    for d in range(15):
      if self.prevLapPieceSpeedLimit[0][31][d] > 6.3 or self.prevLapPieceSpeedLimit[0][31][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[0][31][d] = 6.3
    self.prevLapPieceSpeedLimit[0][31][15] = 6.3
    self.prevLapPieceSpeedLimit[0][31][16] = 6.3
    self.prevLapPieceSpeedLimit[0][31][17] = 6.3
    self.prevLapPieceSpeedLimit[0][31][18] = 6.3
    
    for d in range(0, int(len(self.prevLapPieceSpeedLimit[1][4]))):
      if self.prevLapPieceSpeedLimit[1][4][d] > 6.0 or self.prevLapPieceSpeedLimit[1][4][d] == self.DEFAULT_SPEED_LIMIT:
#         print("Setting speed limit self.prevLapPieceSpeedLimit[1][4][" + str(d) + "]=5.0")
        self.prevLapPieceSpeedLimit[1][4][d] = 6.0 ###
    for d in range(0, int(len(self.prevLapPieceSpeedLimit[1][5]))):
      if self.prevLapPieceSpeedLimit[1][5][d] > 6.0 or self.prevLapPieceSpeedLimit[1][5][d] == self.DEFAULT_SPEED_LIMIT:
#         print("Setting speed limit self.prevLapPieceSpeedLimit[1][5][" + str(d) + "]=5.9")
        self.prevLapPieceSpeedLimit[1][5][d] = 6.0 # 5.9
    # BOT: We crashed l: 1 p: 31 d: 1 sp: 6.79 th 0.0
    for d in range(0, int(len(self.prevLapPieceSpeedLimit[1][30]))):
      if self.prevLapPieceSpeedLimit[1][30][d] > 6.3 or self.prevLapPieceSpeedLimit[1][30][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[1][30][d] = 6.3
    for d in range(16):
      if self.prevLapPieceSpeedLimit[1][31][d] > 6.3 or self.prevLapPieceSpeedLimit[1][31][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[1][31][d] = 6.3
    # BOT: We crashed l: 2 p: 5 d: 34 sp: 6.68 th 0.0
    for d in range(0, int(len(self.prevLapPieceSpeedLimit[2][3]))):
      if self.prevLapPieceSpeedLimit[2][3][d] > 6.3 or self.prevLapPieceSpeedLimit[2][3][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[2][3][d] = 6.3
    for d in range(0, int(len(self.prevLapPieceSpeedLimit[2][4]))):
      if self.prevLapPieceSpeedLimit[2][4][d] > 6.2 or self.prevLapPieceSpeedLimit[2][4][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[2][4][d] = 6.2
    for d in range(0, int(len(self.prevLapPieceSpeedLimit[2][5]))):
      if self.prevLapPieceSpeedLimit[2][5][d] > 6.1 or self.prevLapPieceSpeedLimit[2][5][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[2][5][d] = 6.1
    for d in range(0, int(len(self.prevLapPieceSpeedLimit[2][6]))):
      if self.prevLapPieceSpeedLimit[2][6][d] > 6.4 or self.prevLapPieceSpeedLimit[2][6][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[2][6][d] = 6.4

    for d in range(0, int(len(self.prevLapPieceSpeedLimit[2][15]))):
      if self.prevLapPieceSpeedLimit[2][15][d] > 6.4 or self.prevLapPieceSpeedLimit[2][15][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[2][15][d] = 6.4

    # BOT: We crashed l: 2 p: 31 d: 6 sp: 6.76 th 0.0
    # BOT: We crashed l: 2 p: 31 d: 11 sp: 6.6 th 0.0
    # BOT: We crashed l: 2 p: 31 d: 15 sp: 6.60 th 1.0
    for d in range(0, int(len(self.prevLapPieceSpeedLimit[2][30]))):
      if self.prevLapPieceSpeedLimit[2][30][d] > 6.3 or self.prevLapPieceSpeedLimit[2][30][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[2][30][d] = 6.3
    for d in range(16):
      if self.prevLapPieceSpeedLimit[2][31][d] > 6.3 or self.prevLapPieceSpeedLimit[2][31][d] == self.DEFAULT_SPEED_LIMIT:
        self.prevLapPieceSpeedLimit[2][31][d] = 6.3

  def backPropagateSpeedLimits(self):
    laps = len(self.prevLapPieceSpeedLimit)
    pieces = len(self.prevLapPieceSpeedLimit[0])
    nextLimit = 20
    print("backPropagateSpeedLimits", "laps", laps, "pieces", pieces)
#     self.overrideSpeedLimits()
    for lap in range(laps -1, -1, -1):
#       print("backPropagateSpeedLimits lap", lap)
      for piece in range(pieces -1, -1, -1):
#         print("backPropagateSpeedLimits lap", lap, "piece", piece)
        distances = len(self.prevLapPieceSpeedLimit[lap][piece])
        for inPieceDistance in range(distances -1, -1, -1):
#           if lap == 2 and (piece == 4 or piece == 5):
#             print("lap 2 piece", piece, "d", inPieceDistance, "speedLimit", self.getSpeedLimit(lap, piece, inPieceDistance))
          limit = self.prevLapPieceSpeedLimit[lap][piece][inPieceDistance]
          if False and self.isCornerPiece(piece) and self.getPieceRadius(piece) < 150:
            # set limit to a constant along length of a corner
#             if limit > nextLimit or limit == self.DEFAULT_SPEED_LIMIT:
#               limit = nextLimit
            self.prevLapPieceSpeedLimit[lap][piece][inPieceDistance] = self.CONST_CORNER_SPEED_LIMIT
          else:
            # could still be DEFAULT_SPEED_LIMIT along a straight
            if limit > nextLimit or limit == self.DEFAULT_SPEED_LIMIT:
              maxNextLimitFullBraking = limit * (1 + self.CONST_V)
              if maxNextLimitFullBraking > nextLimit or limit == self.DEFAULT_SPEED_LIMIT:
                # reset limit if too fast or is default along a straight
                limit = nextLimit / (1 + self.CONST_V)
                self.prevLapPieceSpeedLimit[lap][piece][inPieceDistance] = limit
          nextLimit = limit
    
  def msg(self, msg_type, data):
    self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.gametick}))

  def send(self, msg):
    self.socket.send((msg + "\n").encode('utf-8'))
#     print("sent", msg)

  def join(self):
    if self.CONFIG == "final":
      return self.msg("join", {"name": self.name, "key": self.key})    
    else:
#         print("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": self.TRACK, "carCount": self.CAR_COUNT})
      return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": self.TRACK, "carCount": self.CAR_COUNT})

  # example: {"msgType": "throttle", "data": 1.0}
  def throttle(self, throttle):
    self.curThrottle = throttle
    self.changeInThrottle = self.curThrottle-self.prevThrottle
    self.msg("throttle", throttle)
    #print("tick",self.gametick,"throttling",round(throttle,4))


  # Displays elapsed seconds of real time and how many real seconds elapsed per game tick (on average)
  def printRealTimeStats(self):
    realTimeEndSeconds = self.getTime()
    realTimeElapsedSeconds = realTimeEndSeconds - self.realTimeStartSeconds
    realTimeSecondsPerTick = realTimeElapsedSeconds/self.gametick
    print("Real Time performance:", self.gametick, "ticks", round(realTimeElapsedSeconds,3), "seconds", round(realTimeSecondsPerTick, 4), "sec/tick")


  # Turbo multiplies the power of your car's engine for a short period. The server indicates the availability of turbo using the turboAvailable message.
  #
  # {"msgType": "turboAvailable", "data": {
  #   "turboDurationMilliseconds": 500.0,
  #   "turboDurationTicks": 30,
  #   "turboFactor": 3.0
  # }}
  # You can use your turbo boost any time you like after the server has indicated the availability of it. Just like this.
  # 
  # {"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"}
  # Once you turn it on, you can't turn it off. It'll automatically expire in 30 ticks (or what ever the number was in the turboAvailable message).
  def turbo(self):
    if self.isTurboAvailable:
      self.isTurboAvailable = False
      self.isTurboActive = True
      self.turboTick = self.gametick
      print("BOT: Turbo engaged!")
      self.msg("turbo", "Vrrrooooooooom!")
      
  # example: {"msgType": "switchLane", "data": "Left"}    
  def switch(self, mydir):
    if self.haveSwitchedThisPiece == 0:
        lane=self.getLane()
        #left-most lane, so can only go right
        if lane==0 and mydir == 'Left': return
        
        #right-most lane, so can only go left
        if lane==self.getNumLanes()-1 and mydir == 'Right': return
        
        #in the middle lane can switch either direction
        self.haveSwitchedThisPiece = 1
        self.msg("switchLane", mydir)
        print('t',self.gametick,'switching',mydir)

  def ping(self):
    self.msg("ping", {})

  def run(self):
    self.join()
    self.msg_loop()

  def setSpeedLimits(self, a):
    self.prevLapPieceSpeedLimit = a

  def getSpeedLimits(self):
    return self.prevLapPieceSpeedLimit

  def on_join(self, data):
    print("BOT: Joined")
    # print(data)
#     self.ping()

  def on_joinRace(self, data):
    print("BOT: Joined Race", data)
#     self.ping()

  def on_yourCar(self, data):
    print("BOT: YourCar:")
    print(data)
    self.carId = data
    self.carIdx = 0

    
  def on_game_start(self, data):
    print("BOT: Race started")
    self.realTimeStartSeconds = self.getTime()
    self.throttle(1.0) # required to prevent CI Build timeout on tick 0?
    
  def on_game_init(self, data):
    print("BOT: in game init")
    print(data)
    
    # set some global variables        
    #NOTE: since a race does this twice we need to reset all the variables!    
    self.raceData = data  # contains all data about the race            
    self.gametick = 0
    self.haveSwitchedThisPiece = 0
    self.isTurboAvailable = False
    self.isTurboActive = False
    self.curThrottle = 0
    self.prevThrottle = 0				
    self.previousCarData = {}
    self.distanceTravelled = 0  
    self.prevSpeed = 0
    self.speed = 0
    self.prevCarAngle = 0  
    self.angleVelocity = 0
    self.firstCall = True
    self.waitingForRespawn = False    
    self.carIdx = self.getCarIndex(self.carId, data['race']['cars'])
    
    self.ping()

    
  def on_crash(self, data):
    # Only care about our own crash, not other cars
    if data == self.carId:
      l = self.getLap()
      p = self.getPieceID()
      d = self.getInPieceDistance()
      ac = self.acceleration
      av = self.angleVelocity
      ca = self.carAngle
      speedLimit = self.getSpeedLimit(l, p, d)
      print("BOT: We crashed l:", l, "p:", p, "d:", int(d), "sp:", round(self.speed, 2), "spLim:", round(speedLimit, 2), "th:", self.curThrottle, "ca:", round(ca, 2), "av:", round(av, 2))
      self.waitingForRespawn = True
      self.speed = 0
      self.acceleration = 0
      self.angleVelocity = 0
      self.ping()    
      
    
  def on_spawn(self, data):
    # Only care about our own spawn, not other cars
    if data == self.carId:
      print("BOT: Re-spawned", data)
      self.waitingForRespawn = False
      self.throttle(1.0) # required to avoid timeout on CI Build?    

  def on_lapFinished(self, data):
    print("BOT: Lap Finished", data)
#     print(self.myRound(self.prevLapPieceSpeedLimit), file=self.speedFile)
#     print("Track throttle cache", self.prevLapPieceSpeedLimit)

  def on_game_end(self, data):
    print("BOT: Race ended", data)
#     self.ping()

  def on_error(self, data):
    print("Error: {0}".format(data))
    self.ping()

  def on_turbo_available(self, data):
    self.isTurboAvailable = True
    self.turboDuration = data['turboDurationTicks']
    self.turboFactor = data['turboFactor']
    print("Turbo Available!")
#     print("    gameTick=" + str(self.gametick) + " duration=" + str(data['turboDurationMilliseconds']) + "ms " + str(data['turboDurationTicks']) + " ticks, factor=" + str(data['turboFactor']))
#     self.printRealTimeStats()

  def on_touramentEnd(self, data):
    print("Tournament End", data)
#     self.printRealTimeStats()

  def msg_loop(self):
    msg_map = {
        'join': self.on_join,
        'joinRace': self.on_joinRace,
				'yourCar': self.on_yourCar,
        'gameStart': self.on_game_start,
        'gameInit': self.on_game_init,
        'carPositions': self.on_car_positions,
        'crash': self.on_crash,
				'spawn': self.on_spawn,
        'lapFinished': self.on_lapFinished,
        'gameEnd': self.on_game_end,
        'error': self.on_error,
        'turboAvailable': self.on_turbo_available,
				'tournamentEnd': self.on_touramentEnd,
    }
    socket_file = s.makefile()
    line = socket_file.readline()
    while line:
#       print("Recv", line)
      msg = json.loads(line)
      msg_type, data = msg['msgType'], msg['data']
      if 'gameTick' in msg:
        self.gametick = msg['gameTick']            
      if msg_type in msg_map:
#         print("Got {0}".format(msg_type) + " tick=" + str(self.gametick))
        msg_map[msg_type](data)
      else:
        self.ping()
#         print("Got {0}".format(msg_type), "t", self.gametick, "time", round(self.gametick / 60.0, 3))
      line = socket_file.readline()        
      
### GET METHODS ###
  
  
# raceData #
  #TODO: use these!
  def isSessionQualifying(self):
    return ('durationMs' in self.raceData['race']['raceSession'])

  def isSessionRacing(self):
    return ('laps' in self.raceData['race']['raceSession'])

  def getCarIndex(self, carId, carsData):
    idx = 0
    for car in carsData:
      if car['id'] == carId:
        print("getCarIndex(", carId, "carsData) = ", idx)
        return idx
      idx += 1
    print("getCarIndex(", carId, "carsData) FAILED to match car")

  def doesPieceHaveAttribute(self, pieceID, attribute):    
    return (attribute in self.raceData['race']['track']['pieces'][pieceID])
            
  def isCornerPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'angle')
    
  def isStraightPiece(self, pieceID):
    return not(self.isCornerPiece(pieceID))
    
  # Additionally, there may be a switch attribute that indicates that this
  # piece is a switch piece, i.e. you can switch lanes here.      
  def isSwitchPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'switch')      
    
  def getPieceAttribute(self, pieceID, attribute):
    return self.raceData['race']['track']['pieces'][pieceID][attribute]
    
  # NOTE: assume piece is a corner piece  
  # The angle attribute is the angle of the circular segment that the
  # piece forms. For left turns, this is a negative value. For a 45-degree
  # bend to the left, the angle would be -45. For a 30-degree turn to the
  # right, this would be 30.    
  def getPieceAngle(self, pieceID):      
    return self.getPieceAttribute(pieceID, 'angle')
              
  def getNextPieceID(self, pieceID):
    return (pieceID + 1) % self.getNumPieces()
    
  def getNumPieces(self):
    return len(self.raceData['race']['track']['pieces'])  
    
  # For a Bend, there's radius that depicts the radius of the circle
  # that would be formed by this kind of pieces. More specifically,
  # this is the radius at the center line of the track.      
  def getPieceRadius(self, pieceID):
    return self.getPieceAttribute(pieceID, 'radius')
    
  # NOTE: assume lanes are in increasing order of lane index
  # The track may have 1 to 4 lanes. The lanes are described in
  # the protocol as an array of objects that indicate the lane's
  # distance from the track center line. A positive value tells
  # that the lanes is to the right from the center line while a
  # negative value indicates a lane to the left from the center.
  def getLaneDistFromCenter(self, lane):
    return self.raceData['race']['track']['lanes'][lane]['distanceFromCenter']
    
    
  def getNumLanes(self):
    return len(self.raceData['race']['track']['lanes'])
  
  def getNumLaps(self):
    return self.raceData['race']['raceSession']['laps']

  # For a Straight the length attribute depicts the length of the piece.
  def getPieceLength(self, pieceID, lane=0):
    if self.isCornerPiece(pieceID):    
      return self.getCurveLength(pieceID, lane)    
    return self.getPieceAttribute(pieceID, 'length')
    
    
  # NOTE: assume piece is curved
  def getCurveLength(self, pieceID, lane=0):
    angle = self.getPieceAngle(pieceID)
    length = self.getCurveRadius(pieceID, lane) * 2 * math.pi * abs(angle) / 360.0
    #assert length >= 0
    
    return length

    
  # NOTE: assume piece is curved
  def getCurveRadius(self, pieceID, lane):    
    angle = self.getPieceAngle(pieceID)
    mult = -1
    if angle < 0: mult = +1  # left turn
    radius = self.getPieceRadius(pieceID) + mult * self.getLaneDistFromCenter(lane)
    #assert radius >= 0

    return radius    
    
# carData #

  # zero based index of the piece the car is on
  def getPieceID(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['piecePosition']['pieceIndex']              
    else:
      return data[self.carIdx]['piecePosition']['pieceIndex']           
          
    
  # the distance the car's Guide Flag (see above) has travelled from the
  # start of the piece along the current lane    
  # So I guess no need to complex computations for curves?    
  def getInPieceDistance(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['piecePosition']['inPieceDistance']          
    else:
      return data[self.carIdx]['piecePosition']['inPieceDistance']    
    
     
  # the number of laps the car has completed. The number 0 indicates
  # that the car is on its first lap. The number -1 indicates that it
  # has not yet crossed the start line to begin it's first lap.       
  def getLap(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['piecePosition']['lap']      
    else:
      return data[self.carIdx]['piecePosition']['lap']
             
    
  # TODO: assumes startLane==endLane
  # a pair of lane indices. Usually startLaneIndex and endLaneIndex
  # are equal, but they do differ when the car is currently switching lane    
  def getLane(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['piecePosition']['lane']['startLaneIndex']      
    else:
      return data[self.carIdx]['piecePosition']['lane']['startLaneIndex']
    
          
  # The angle depicts the car's slip angle. Normally this is zero, but
  # when you go to a bend fast enough, the car's tail will start to drift.
  # Naturally, there are limits to how much you can drift without crashing
  # out of the track.            
  def getCarAngle(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['angle']                
    else:      
      return data[self.carIdx]['angle']          
    
    
    
  # distance travelled since last tick
  def getDistanceTravelled(self):  
  
    # start of race, there is no distance travelled yet
    if self.previousCarData == {}: return 0

    curPieceID = self.getPieceID()
    previousPieceID = self.getPieceID(self.previousCarData)
  
    curPieceDistance = self.getInPieceDistance()      
    previousPieceDistance = self.getInPieceDistance(self.previousCarData)
    
  
    previousLane = self.getLane(self.previousCarData)
  
    distToStartOfCurPiece = curPieceDistance      
    distToStartOfPreviousPiece = previousPieceDistance        
  
    prevPieceLength = self.getPieceLength(previousPieceID, previousLane)    
    
    distToEndOfPreviousPiece = prevPieceLength - distToStartOfPreviousPiece        
    #IF THIS ASSERT FAILS then there is something wrong... please record where it fails
    # fails after pre-corner pieceID = 17 on USA (lap=0 tick=282 d=1660.6917330181439) 
    # note server reported piece distance measurements are all based on the centre line-length
    # using lane-length calculations might be causing error.
    #assert distToEndOfPreviousPiece >= 0
  
    # if on the same piece
    if curPieceID == previousPieceID:
      #assert distToStartOfCurPiece - distToStartOfPreviousPiece >= 0
      return distToStartOfCurPiece - distToStartOfPreviousPiece
     
    # on a new track piece
    self.haveSwitchedThisPiece = 0
    return distToStartOfCurPiece + distToEndOfPreviousPiece
    
    
  def getTime(self):
    #return time.clock()
    return time.time()    
    
    
  def RadToDeg(self, a):
    return a/math.pi*180.0
  
  
  def DegToRad(self, a):
    return a/180.0*math.pi
    
    

### AI METHODS ###

  def on_car_positions(self, data):  
  
    self.OLD_TIME=self.getTime()
  
    self.carData = data  
    # print(data)
  
    self.updateBeforeThrottle()

    if self.MODE=='QUAL':
      self.doQualification()
    elif self.MODE=='RACE':
      if not self.finalSprint():  
        self.speedLimitObserver()
    self.updateAfterThrottle()
    
     
#############################################################################

  def finalSprint(self):
    # Turbo and full throttle at the final finish (-7 finland, -5 usa)
    if self.isSessionQualifying(): return False
    if not self.isSessionRacing(): return False
    
    numLaps = self.getNumLaps()
    
    pieceID = self.getPieceID()
    if self.getLap()==numLaps-1 and pieceID>=self.getNumPieces()-6 and not self.waitingForRespawn:
      print("final finish turbo sprint " + str(pieceID))
      self.turbo()
      angle = abs(self.getCarAngle())
      if angle > 25 and self.isTurboActive:
          self.throttle(1.1 - 0.01 * angle)
      else:
          self.throttle(1.0)
      return True
    return False

    
  def doQualification(self):
    
    pieceID = self.getPieceID()    

    #set a largish constant throttle, so that we get a carAngle>0 on a corner
    self.throttle(self.CONST_QUALIFY_THROTTLE)                
    
    if self.isCornerPiece(pieceID) and abs(self.getCarAngle()) > self.EPS and self.Fslip==-1:
      lane=self.getLane()
      V=self.speed
      R=self.getCurveRadius(pieceID, lane)
      A=self.getCarAngle()
      B=self.RadToDeg(V/R)
      Bslip=B-A
      Vthresh=self.DegToRad(Bslip)*R
      self.Fslip=Vthresh*Vthresh/R      #the most important value of all time!!!
      print('t',self.gametick,'Fslip',self.Fslip)
      
            
    if self.isStraightPiece(pieceID) and abs(self.getCarAngle()) < self.EPS and self.prevSpeed > self.EPS:
    #if abs(self.prevSpeed) > self.EPS:
      self.STATS.append({'s':self.prevSpeed, 'a':self.acceleration, 't':self.prevThrottle})      
            
    if len(self.STATS)==2:    
      print(self.STATS)
      st1=self.STATS[0]
      st2=self.STATS[1]            
      
      self.CONST_V=(st2['a']*st1['t']-st1['a']*st2['t'])/(st2['s']*st1['t']-st1['s']*st2['t'])
      self.CONST_T=(st1['a']-self.CONST_V*st1['s'])/st1['t']
      self.foundCONST=True
      print("t", self.gametick, "speedNew", str(round(self.CONST_T,3))+"*throttleOld + ", str(round(1+self.CONST_V,3))+"*speedOld")
    
    #now we can race!
    if self.Fslip!=-1 and self.foundCONST:
      self.MODE='RACE'
      print('t',self.gametick,'activating Race mode!!!!!')
    
    
  def updateBeforeThrottle(self):
  
    #deactive turbo
    if self.isTurboActive:
      print('tick',self.gametick,'turbo is active')
      if self.gametick-self.turboTick >= self.turboDuration:
        self.isTurboActive=False
        print('tick',self.gametick,'turning turbo off')
        
      
    self.dist = self.getDistanceTravelled()  # distance travelled since last tick
    self.distanceTravelled += self.dist      # total distance travelled
    self.speed = self.dist / 1               # instantaneous speed. NOTE: assume we didn't miss any ticks
    self.acceleration = (self.speed - self.prevSpeed) / 1
    # average speed    
    if self.gametick == 0:
      self.averageSpeed = 0        
    else:
      self.averageSpeed = self.distanceTravelled / self.gametick  
      
    self.carAngle=self.getCarAngle()
    self.angleVelocity=self.carAngle-self.prevCarAngle       
    
  def updateAfterThrottle(self):
#     lap = self.getLap()
#     piece = self.getPieceID()
#     d = self.getInPieceDistance()
#     if lap == 2 and piece == 4 or piece == 5:
#       print("lap 2 piece", piece, "d", d, "speed", self.speed, "speedLimit", self.getSpeedLimit(lap, piece, d), "th", self.curThrottle)
    self.previousCarData = self.carData      
    self.prevSpeed = self.speed
    self.prevCarAngle = self.carAngle
    self.prevThrottle = self.curThrottle
    
#     print("distance,speed,throttle", file=self.speedFile)
    print(str(self.distanceTravelled) + "," + str(self.speed) + "," + str(self.curThrottle) + "," + str(self.carAngle), file=self.speedFile)


    
    
  #drive at a constant speed
  #constant speed occurs when T*throttle+V*speed=0
  def AI_constantSpeed(self):
    speed=5
    th=-self.CONST_V*speed/self.CONST_T/math.cos(self.DegToRad(self.carAngle))
    self.throttle(th)
      

############## Simulator ########################
    
    
  def speedLimitObserver(self):
        
    curPieceID = self.getPieceID()
    
    # No need to check for crash etc when waiting to re-spawn. just ping   
    if self.waitingForRespawn:
      self.ping()
      return    
    
    nextPieceID = self.getNextPieceID(curPieceID)        
    if self.isSwitchPiece(nextPieceID):
      fastLane = self.getFastLane(self.getLap(), nextPieceID)
      lane = self.getLane()
      if fastLane < lane:
        self.switch('Left')
      elif fastLane > lane:
        self.switch('Right')
        
#       # switch lanes if it is faster  
#       nextPieceID2 = self.getNextPieceID(nextPieceID)
#       if self.isCornerPiece(nextPieceID2):
#         nextPieceID3 = self.getNextPieceID(nextPieceID2)
#         
#         if self.getPieceAngle(nextPieceID2) < 0 and self.getPieceAngle(nextPieceID3) > 0:   
#           # next piece turns left but following piece turns right        
#           self.switch('Left')
#         elif self.getPieceAngle(nextPieceID2) < 0:
#           # next piece turns left and following piece is straight or turns left        
#           self.switch('Right')
#         else:
#           # next piece turns right       
#           self.switch('Left')          
# 
# #       #switch in random places
# #           if random.random() < 0.5:
# #             self.switch('Left')
# #           else:
# #             self.switch('Right')
        
#     # Always travel constant speed around curves, regardless of speed limits:
#     if self.isCornerPiece(curPieceID) and self.speed > 5 and self.getPieceRadius(curPieceID) < 150:
#       th=-self.CONST_V*self.speed/self.CONST_T/math.cos(self.DegToRad(self.carAngle))
#       if th > 1.0: th = 1.0
#       th *= self.CONST_CORNER_THROTTLE_REDUCE
#       self.throttle(th)
#       return

    # Determine speed limit at current position
    lap = self.getLap()
    inPieceDistance = self.getInPieceDistance()
    speedLimit = self.getSpeedLimit(lap, curPieceID, inPieceDistance)
    limit1 = self.speedThresholdFactor * speedLimit
    limit2 = (1 + self.CONST_V) * limit1
    limit3 = limit2 - 1.9 ########## D
    if self.speed >= limit2:
      self.throttle(0.0)
    elif self.speed >= limit3:
      self.throttle((limit1 - self.speed)/limit1)
    else:
      self.throttle(1.0)

   
  
  #round every element in a to some decimals
  def myRound(self, a, decimals=3):
    for i in range(len(a)): a[i]=round(a[i], decimals)  
    return a 
  
  def score(self, endState):
    return endState['tick']+1000000*endState['piecesLeft']
    
    
  def simulate(self, commands, startState, maxTicks):
    n=len(commands['th'])
    
    curState=copy.deepcopy(startState)
    #print 'start',curState    
    
    tick=0
    i=0
      
    while i<n and tick<maxTicks:

      tick+=1
      pieceID=curState['piece']
      
      throttle=commands['th'][i]
            
      #use turbo      
      if commands['turbo'] and self.isTurboAvailable:
        if tick<=self.turboDuration:
          throttle=throttle*self.turboFactor               
      if self.isTurboActive:
        if self.gametick+tick-self.turboTick<=self.turboDuration:
          throttle=throttle*self.turboFactor             
      
      pieceLength=self.getPieceLength(pieceID,curState['lane'])
      
      if curState['pieceDistance']+curState['speed'] < pieceLength:
        curState['pieceDistance']+=curState['speed']
        curState['speed']+=self.CONST_T*throttle+self.CONST_V*curState['speed']
      else:
        extraDist=curState['pieceDistance']+curState['speed'] - pieceLength
        curState['pieceDistance']=extraDist
        curState['speed']+=self.CONST_T*throttle+self.CONST_V*curState['speed']
        curState['piece']=self.getNextPieceID(curState['piece'])
        i+=1

      # Update the angle and angular velocity
      if self.SLIP_MODEL==1 or self.SLIP_MODEL==2:

        alpha  = -self.CONST_A1 * curState['angleVelocity']
        alpha -= self.CONST_A2 * curState['speed'] * curState['angle']

        if self.SLIP_MODEL==1:
          if self.isCornerPiece(pieceID):
            curveRadius=self.getCurveRadius(pieceID,curState['lane'])
            
            tmp = (curState['speed'] - self.CONST_R1*curveRadius - self.CONST_R2)
            if (tmp > 0):
              if (self.getPieceAngle(pieceID) < 0):
                alpha -= self.CONST_M * tmp
              else:
                alpha += self.CONST_M * tmp
                
                
        elif self.SLIP_MODEL==2:                  
          if self.isCornerPiece(pieceID):
            curveRadius=self.getCurveRadius(pieceID,curState['lane'])
          
            tmp = curState['speed']/curveRadius         
            #tmp = curState['speed']*curState['speed']/curveRadius
            if (tmp > self.CONST_B1):
              if (self.getPieceAngle(pieceID) < 0):
                alpha -= self.CONST_B2 * (tmp - self.CONST_B1)
              else:
                alpha += self.CONST_B2 * (tmp - self.CONST_B1)   

        curState['angleVelocity'] += alpha
        curState['angle'] += curState['angleVelocity']

      #print tick,curState
      
      #check crashes        
      if self.isCornerPiece(pieceID):
        if self.SLIP_MODEL==3:
          curveRadius=self.getCurveRadius(pieceID,curState['lane'])        
          Vmax=math.sqrt(self.Fslip * curveRadius * self.DANGER_FACTOR)
          if curState['speed'] >= Vmax:
            curState['tick']=tick
            curState['crash']=True
            curState['piecesLeft']=max(1,n-i)      #this includes the current piece, max just in case
            curState['finishedN']=i
            return curState          
        
        else:
          if abs(curState['angle']) > self.MAX_SLIP_ANGLE:
            #print 'CRASH!'
            curState['tick']=tick
            curState['crash']=True
            curState['piecesLeft']=max(1,n-i)      #this includes the current piece, max just in case
            curState['finishedN']=i
            return curState
        
    
    #print "woo!"
    curState['tick']=tick
    curState['crash']=False
    curState['piecesLeft']=n-i
    curState['finishedN']=i
    return curState
    
    
  def shuffle(self, a):
    n=len(a)
    for i in range(n):
      k=int(random.random()*(n-i)+i)
      temp=a[i]
      a[i]=a[k]
      a[k]=temp      
        


if __name__ == "__main__":
  if len(sys.argv) != 5:
    print("Usage: ./run host port botname botkey")
  else:
    host, port, name, key = sys.argv[1:5]
    bestLapPieceSpeedLimits = []
#     while True:
    print("SpeedBot1 Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = SpeedBot1(s, name, key)
    if bestLapPieceSpeedLimits != []:
      bot.setSpeedLimits(bestLapPieceSpeedLimits)
    else:
      keimolaFile = open("stats/keimola-0645.json", "r")
      
      bot.setSpeedLimitsFromReplayFile(keimolaFile)
#       bot.setDefaultSpeedLimitsFromReplayFile(keimolaFile)
      bestLapPieceSpeedLimits = bot.getSpeedLimits()
#       saveFileInit = open("stats/keimolaSpeedLimitInit1.py", "w")
#       for lapData in bestLapPieceSpeedLimits:
#         print("lapData = ", file=saveFileInit)
#         json.dump(lapData, saveFileInit, indent=0) # indent 2 file truncated

#       bot.overrideSpeedLimits()

#       saveFileInit = open("stats/keimolaSpeedLimitInit2.py", "w")
      bestLapPieceSpeedLimits = bot.getSpeedLimits()
#       for lapData in bestLapPieceSpeedLimits:
#         print("lapData = ", file=saveFileInit)
#         json.dump(lapData, saveFileInit, indent=0) # indent 2 file truncated

      bot.backPropagateSpeedLimits()
      bestLapPieceSpeedLimits = bot.getSpeedLimits()
      saveFileInit = open("stats/keimolaSpeedLimitInit3.py", "w")
      bestLapPieceSpeedLimits = bot.getSpeedLimits()
      for lapData in bestLapPieceSpeedLimits:
#         print("lapData = ", file=saveFileInit)
        json.dump(lapData, saveFileInit, indent=0) # indent 2 file truncated
    bot.run()
    print("Bot terminated!")
    bestLapPieceSpeedLimits = bot.getSpeedLimits()
    saveFile = open("stats/keimolaSpeedLimit.py", "w")
    for lapData in bestLapPieceSpeedLimits:
#       print("lapData = ", file=saveFile)
      json.dump(lapData, saveFile, indent=0) # indent 2 file truncated
#     time.sleep(600)
