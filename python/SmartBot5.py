import json
import socket
import sys
import math
import random
import copy
import time
#import matrix
#import numpy
#import thread


#TODO
# - compute switch distance, should be around 102, see http://www.reddit.com/r/HWO/comments/23lorl/inpiecedistance_longer_than_piece_length/
# - add switching. At first, switch lanes when there is someone in front of us
# - use fewer commands before the race finish
# - use multithreading?
# - add computation of R1, R2, A1, A2 and M during qualification period
# - ping could be harmful: http://www.reddit.com/r/HWO/comments/23z1hh/why_does_car_first_moves_in_tick_3/


class SmartBot(object):

### GLOBAL VARIABLES ###

  ##IMPORTANT VARIABLE!!!
  CONFIG="final"    #for final submission to the server
  #CONFIG="test"     #for testing  

# track selection #
  TRACK="finland"
  #TRACK="germany"    
  #TRACK="usa"
  #TRACK="france"      
  CAR_COUNT = 1
  
  #QUAL: qualification mode, which computes all the physics constants
  #RACE: race mode
  MODE='QUAL'    
  
# physics model #

  STATS=[]    #for speed model
    
  
  #speedNew = speedOld + const_T*throttleOld + const_V*speedOld  
  CONST_T=0.2
  CONST_V=-0.02
  
  #1 uses R1, R2 and M
  #2 uses B1 and B2. DOESN'T WORK!!! DONT USE!!!
  #3 from http://www.reddit.com/r/HWO/comments/245yms/how_to_calculate_speed_at_which_you_start_slipping/
  #   this uses Fslip and DANGER_FACTOR. Doesn't use MAX_SLIP_ANGLE
  SLIP_MODEL=3

  CONST_A1=0.1
  CONST_A2=0.00125
  
  CONST_R1=0.0325
  CONST_R2=2.417
  CONST_M=0.5
  
  CONST_B1=0.4
  CONST_B2=2.8  
  
  Fslip=-1
  foundCONST=False
    
  #how much extra speed (in %) are we willing to take in corners. 1 is no danger
  #1.4 crashes on finland
  #1.2 crashes on germany
  #1.2 works on finland
  DANGER_FACTOR=1.1     

  
  MAX_SLIP_ANGLE = 45 
  
  NO_TURBO = False     #if true then set turbo off!
  
  #max allowed time since carPosition message in seconds
  MAX_TIME=0.5

  #optimizer parameters
  STEP = 0.1        #should divide evenly into 1
  LOOK_AHEAD = 10   #how many pieces do we look ahead?
  
  
  SWITCH_PROBABILITY = 0.10
  
  EPS = 1e-9    
  
  gametick=0
  
# various data and constants #  
    
  def __init__(self, socket, name, key):
    # set some global variables				
    self.socket = socket
    self.name = name
    self.key = key
    
    self.prevCommands={'th':[],'turbo':False}
    for i in range(self.LOOK_AHEAD): self.prevCommands['th'].append(self.STEP)
    

  def msg(self, msg_type, data):
    self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.gametick}))

  def send(self, msg):
    self.socket.send((msg + "\n").encode('utf-8'))

  def join(self):
    if self.CONFIG == "final":
      return self.msg("join", {"name": self.name, "key": self.key})    
    else:
      if self.TRACK == "finland":
        return self.msg("join", {"name": self.name, "key": self.key})
      else:
        return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": self.TRACK, "carCount": self.CAR_COUNT})

  # example: {"msgType": "throttle", "data": 1.0}
  def throttle(self, throttle):
    self.curThrottle = throttle
    self.changeInThrottle = self.curThrottle-self.prevThrottle
    self.msg("throttle", throttle)
    #print("tick",self.gametick,"throttling",round(throttle,4))


  # Displays elapsed seconds of real time and how many real seconds elapsed per game tick (on average)
  def printRealTimeStats(self):
    realTimeEndSeconds = self.getTime()
    realTimeElapsedSeconds = realTimeEndSeconds - self.realTimeStartSeconds
    realTimeSecondsPerTick = realTimeElapsedSeconds/self.gametick
    print("Real Time performance:", self.gametick, "ticks", round(realTimeElapsedSeconds,3), "seconds", round(realTimeSecondsPerTick, 4), "sec/tick")
 	
  	
  # Turbo multiplies the power of your car's engine for a short period. The server indicates the availability of turbo using the turboAvailable message.
  #
  # {"msgType": "turboAvailable", "data": {
  #   "turboDurationMilliseconds": 500.0,
  #   "turboDurationTicks": 30,
  #   "turboFactor": 3.0
  # }}
  # You can use your turbo boost any time you like after the server has indicated the availability of it. Just like this.
  # 
  # {"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"}
  # Once you turn it on, you can't turn it off. It'll automatically expire in 30 ticks (or what ever the number was in the turboAvailable message).
  def turbo(self):
    if self.isTurboAvailable:
      self.isTurboAvailable = False
      self.isTurboActive = True
      self.turboTick = self.gametick
      print("BOT: Turbo engaged!")
      self.msg("turbo", "Vrrrooooooooom!")
      
  # example: {"msgType": "switchLane", "data": "Left"}    
  def switch(self, mydir):
    if self.haveSwitchedThisPiece == 0:
      self.haveSwitchedThisPiece = 1
      self.msg("switchLane", mydir)
      print('tick',self.gametick,'switching',mydir)

  def ping(self):
    self.msg("ping", {})

  def run(self):
    self.join()
    self.msg_loop()

  def on_join(self, data):
    print("BOT: Joined")
    # print(data)
    self.ping()

  def on_yourCar(self, data):
    print("BOT: YourCar:")
    print(data)
    self.carId = data
    self.carIdx = 0

    
  def on_game_start(self, data):
    print("BOT: Race started")
    self.realTimeStartSeconds = self.getTime()
    self.throttle(1.0) # required to prevent CI Build timeout on tick 0?
    
  def on_game_init(self, data):
    print("BOT: in game init")
    print(data)
    
    # set some global variables        
    #NOTE: since a race does this twice we need to reset all the variables!    
    self.raceData = data  # contains all data about the race            
    self.gametick = 0
    self.haveSwitchedThisPiece = 0
    self.isTurboAvailable = False
    self.isTurboActive = False
    self.curThrottle = 0
    self.prevThrottle = 0				
    self.previousCarData = {}
    self.distanceTravelled = 0  
    self.prevSpeed = 0
    self.speed = 0
    self.prevCarAngle = 0  
    self.angleVelocity = 0
    self.firstCall = True
    self.waitingForRespawn = False    
    self.carIdx = self.getCarIndex(self.carId, data['race']['cars'])    
    self.ping()
      

      
  def on_crash(self, data):
    print("BOT: We crashed")
    self.waitingForRespawn = True
    self.speed = 0
    self.acceleration = 0
    self.angleVelocity = 0
    self.ping()

  def on_spawn(self, data):
    print("BOT: Re-spawned")
    self.waitingForRespawn = False
    self.throttle(1.0) # required to avoid timeout on CI Build?

  def on_game_end(self, data):
    print("BOT: Race ended")
    self.ping()

  def on_error(self, data):
    print("Error: {0}".format(data))
    self.ping()

  def on_turbo_available(self, data):
    self.isTurboAvailable = True
    self.turboDuration = data['turboDurationTicks']
    self.turboFactor = data['turboFactor']
    print("Turbo Available!")
    print("    gameTick=" + str(self.gametick) + " duration=" + str(data['turboDurationMilliseconds']) + "ms " + str(data['turboDurationTicks']) + " ticks, factor=" + str(data['turboFactor']))
    self.printRealTimeStats()

  def on_touramentEnd(self, data):
    print("Tournament End")
    self.printRealTimeStats()

  def msg_loop(self):
    msg_map = {
        'join': self.on_join,
				'yourCar': self.on_yourCar,
        'gameStart': self.on_game_start,
        'gameInit': self.on_game_init,
        'carPositions': self.on_car_positions,
        'crash': self.on_crash,
				'spawn': self.on_spawn,
        'gameEnd': self.on_game_end,
        'error': self.on_error,
        'turboAvailable': self.on_turbo_available,
				'tournamentEnd': self.on_touramentEnd,
    }
    socket_file = s.makefile()
    line = socket_file.readline()
    while line:
      msg = json.loads(line)
      msg_type, data = msg['msgType'], msg['data']
      if 'gameTick' in msg:
        self.gametick = msg['gameTick']            
      if msg_type in msg_map:
        #print("Got {0}".format(msg_type) + " tick=" + str(self.gametick) + " th=" + str(self.curThrottle) + " d=" + str(self.distanceTravelled) + " sp=" + str(self.prevSpeed))
        msg_map[msg_type](data)
      else:
        print("Got {0}".format(msg_type) + " tick=" + str(self.gametick) + " time=" + str(round(self.gametick / 60.0, 3)))
        self.ping()
      line = socket_file.readline()        
      
### GET METHODS ###
  
  
# raceData #
  #TODO: use these!
  def isSessionQualifying(self):
    return ('durationMs' in self.raceData['race']['raceSession'])

  def isSessionRacing(self):
    return ('laps' in self.raceData['race']['raceSession'])

  def getCarIndex(self, carId, carsData):
    idx = 0
    for car in carsData:
      if car['id'] == carId:
        print("getCarIndex(", carId, "carsData) = ", idx)
        return idx
      idx += 1
    print("getCarIndex(", carId, "carsData) FAILED to match car")
  	
  def doesPieceHaveAttribute(self, pieceID, attribute):    
    return (attribute in self.raceData['race']['track']['pieces'][pieceID])
            
  def isCornerPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'angle')
    
  def isStraightPiece(self, pieceID):
    return not(self.isCornerPiece(pieceID))
    
  # Additionally, there may be a switch attribute that indicates that this
  # piece is a switch piece, i.e. you can switch lanes here.      
  def isSwitchPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'switch')      
    
  def getPieceAttribute(self, pieceID, attribute):
    return self.raceData['race']['track']['pieces'][pieceID][attribute]
    
  # NOTE: assume piece is a corner piece  
  # The angle attribute is the angle of the circular segment that the
  # piece forms. For left turns, this is a negative value. For a 45-degree
  # bend to the left, the angle would be -45. For a 30-degree turn to the
  # right, this would be 30.    
  def getPieceAngle(self, pieceID):      
    return self.getPieceAttribute(pieceID, 'angle')
              
  def getNextPieceID(self, pieceID):
    return (pieceID + 1) % self.getNumPieces()
    
  def getNumPieces(self):
    return len(self.raceData['race']['track']['pieces'])  
    
  # For a Bend, there's radius that depicts the radius of the circle
  # that would be formed by this kind of pieces. More specifically,
  # this is the radius at the center line of the track.      
  def getPieceRadius(self, pieceID):
    return self.getPieceAttribute(pieceID, 'radius')
    
  # NOTE: assume lanes are in increasing order of lane index
  # The track may have 1 to 4 lanes. The lanes are described in
  # the protocol as an array of objects that indicate the lane's
  # distance from the track center line. A positive value tells
  # that the lanes is to the right from the center line while a
  # negative value indicates a lane to the left from the center.
  def getLaneDistFromCenter(self, lane):
    return self.raceData['race']['track']['lanes'][lane]['distanceFromCenter']
    
    
  def getNumLanes(self):
    return len(self.raceData['race']['track']['lanes'])
    
          
  # NOTE: assume piece is straight
  # For a Straight the length attribute depicts the length of the piece.
  def getPieceLength(self, pieceID):
    return self.getPieceAttribute(pieceID, 'length')
    
    
  # NOTE: assume piece is curved
  def getCurveLength(self, pieceID, lane):
    angle = self.getPieceAngle(pieceID)
    length = self.getCurveRadius(pieceID, lane) * 2 * math.pi * abs(angle) / 360.0
    #assert length >= 0
    
    return length

    
  # NOTE: assume piece is curved
  def getCurveRadius(self, pieceID, lane):    
    angle = self.getPieceAngle(pieceID)
    mult = -1
    if angle < 0: mult = +1  # left turn
    radius = self.getPieceRadius(pieceID) + mult * self.getLaneDistFromCenter(lane)
    #assert radius >= 0

    return radius    
    
# carData #

  # zero based index of the piece the car is on
  def getPieceID(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['piecePosition']['pieceIndex']              
    else:
      return data[self.carIdx]['piecePosition']['pieceIndex']           
          
    
  # the distance the car's Guide Flag (see above) has travelled from the
  # start of the piece along the current lane    
  # So I guess no need to complex computations for curves?    
  def getInPieceDistance(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['piecePosition']['inPieceDistance']          
    else:
      return data[self.carIdx]['piecePosition']['inPieceDistance']    
    
     
  # the number of laps the car has completed. The number 0 indicates
  # that the car is on its first lap. The number -1 indicates that it
  # has not yet crossed the start line to begin it's first lap.       
  def getLap(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['piecePosition']['lap']      
    else:
      return data[self.carIdx]['piecePosition']['lap']
             
    
  # TODO: assumes startLane==endLane
  # a pair of lane indices. Usually startLaneIndex and endLaneIndex
  # are equal, but they do differ when the car is currently switching lane    
  def getLane(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['piecePosition']['lane']['startLaneIndex']      
    else:
      return data[self.carIdx]['piecePosition']['lane']['startLaneIndex']
    
          
  # The angle depicts the car's slip angle. Normally this is zero, but
  # when you go to a bend fast enough, the car's tail will start to drift.
  # Naturally, there are limits to how much you can drift without crashing
  # out of the track.            
  def getCarAngle(self, data={}):
    if data == {}:
      return self.carData[self.carIdx]['angle']                
    else:      
      return data[self.carIdx]['angle']          
    
    
    
  # distance travelled since last tick
  def getDistanceTravelled(self):  
  
    # start of race, there is no distance travelled yet
    if self.previousCarData == {}: return 0

    curPieceID = self.getPieceID()
    previousPieceID = self.getPieceID(self.previousCarData)
  
    curPieceDistance = self.getInPieceDistance()      
    previousPieceDistance = self.getInPieceDistance(self.previousCarData)
    
  
    previousLane = self.getLane(self.previousCarData)
  
    distToStartOfCurPiece = curPieceDistance      
    distToStartOfPreviousPiece = previousPieceDistance        
  
    if self.isCornerPiece(previousPieceID):    
      prevPieceLength = self.getCurveLength(previousPieceID, previousLane)    
    else:
      prevPieceLength = self.getPieceLength(previousPieceID)
    
    distToEndOfPreviousPiece = prevPieceLength - distToStartOfPreviousPiece        
    #IF THIS ASSERT FAILS then there is something wrong... please record where it fails
    # fails after pre-corner pieceID = 17 on USA (lap=0 tick=282 d=1660.6917330181439) 
    # note server reported piece distance measurements are all based on the centre line-length
    # using lane-length calculations might be causing error.
    #assert distToEndOfPreviousPiece >= 0
  
    # if on the same piece
    if curPieceID == previousPieceID:
      #assert distToStartOfCurPiece - distToStartOfPreviousPiece >= 0
      return distToStartOfCurPiece - distToStartOfPreviousPiece
     
    # on a new track piece
    self.haveSwitchedThisPiece = 0
    return distToStartOfCurPiece + distToEndOfPreviousPiece
    
    
  def getTime(self):
    #return time.clock()
    return time.time()    
    
    
  def RadToDeg(self, a):
    return a/2/math.pi*360
  
  
  def DegToRad(self, a):
    return a/360*2*math.pi
    
    

### AI METHODS ###

  def on_car_positions(self, data):  
  
    self.OLD_TIME=self.getTime()
  
    self.carData = data  
    # print(data)
  
    self.updateBeforeThrottle()

#     if self.gametick == 0:
#       # DO NOT process any further before the session starts (tick 1)
# 			print("tick",self.gametick,"INITIAL Car Positions")
# 			self.ping()
# 			return
		
    if self.MODE=='QUAL':
      self.doQualification()
    elif self.MODE=='RACE':      
      self.AI_simulator()
    self.updateAfterThrottle()
    
    elapsedTime=self.getTime()-self.OLD_TIME
    #print('tick',self.gametick,'elapsedTime',round(elapsedTime,4))
    
    
   
     
     
#############################################################################
    
    
  def doQualification(self):
    
    pieceID = self.getPieceID()    

    #set a largish constant throttle, so that we get a carAngle>0 on a corner
    self.throttle(0.8)                
    
    if self.isCornerPiece(pieceID) and abs(self.getCarAngle()) > self.EPS and self.Fslip==-1:
      lane=self.getLane()
      V=self.speed
      R=self.getCurveRadius(pieceID, lane)
      A=self.getCarAngle()
      B=self.RadToDeg(V/R)
      Bslip=B-A
      Vthresh=self.DegToRad(Bslip)*R
      self.Fslip=Vthresh*Vthresh/R      #the most important value of all time!!!
      print('tick',self.gametick,'Fslip',self.Fslip)
            
    #if self.isStraightPiece(pieceID) and abs(self.getCarAngle()) < self.EPS and self.prevSpeed > self.EPS:
    if abs(self.prevSpeed) > self.EPS:
      self.STATS.append({'s':self.prevSpeed, 'a':self.acceleration, 't':self.prevThrottle})      
            
    if len(self.STATS)==2:    
      print(self.STATS)
      st1=self.STATS[0]
      st2=self.STATS[1]            
      
      #using numpy
      #a = numpy.array([[st1['t'],st1['s']],[st2['t'],st2['s']]])
      #b = numpy.array([st1['a'],st2['a']])
      #x = numpy.linalg.solve(a, b)            
      #self.CONST_T=x[0]
      #self.CONST_V=x[1]
      
      #using a toy matrix library
      #a = matrix.matrix([[st1['t'],st1['s']],[st2['t'],st2['s']]])
      #b = matrix.vector([st1['a'],st2['a']])
      #x = matrix.solve(a,b)
      #self.CONST_T=x.get((0,0))
      #self.CONST_V=x.get((1,0))      
      
      #using my brain :)
      self.CONST_V=(st2['a']*st1['t']-st1['a']*st2['t'])/(st2['s']*st1['t']-st1['s']*st2['t'])
      self.CONST_T=(st1['a']-self.CONST_V*st1['s'])/st1['t']
      self.foundCONST=True
      print("tick=" + str(self.gametick) + ", speedNew = " + str(round(self.CONST_T,3))+"*throttleOld + " + str(round(1 + self.CONST_V,3))+"*speedOld")
      #self.MODE='RACE'
      
    
    #now we can race!
    if self.Fslip!=-1 and self.foundCONST:
      self.MODE='RACE'
      print('tick',self.gametick,'activating Race mode!!!!!')
    
    
  def updateBeforeThrottle(self):
  
    #deactive turbo
    if self.isTurboActive:
      print('tick',self.gametick,'turbo is active')
      if self.gametick-self.turboTick >= self.turboDuration:
        self.isTurboActive=False
        print('tick',self.gametick,'turning turbo off')
        
      
    self.dist = self.getDistanceTravelled()  # distance travelled since last tick
    self.distanceTravelled += self.dist      # total distance travelled
    self.speed = self.dist / 1               # instantaneous speed. NOTE: assume we didn't miss any ticks
    self.acceleration = (self.speed - self.prevSpeed) / 1
    # average speed    
    if self.gametick == 0:
      self.averageSpeed = 0        
    else:
      self.averageSpeed = self.distanceTravelled / self.gametick  
      
      
    #print 'tick',self.gametick,'dist',self.dist,'speed',self.speed,'accel',self.acceleration

    pieceID = self.getPieceID()    
    
    # find relationship between throttle, speed and acceleration on straight pieces  
    #if self.isStraightPiece(pieceID) and abs(self.getCarAngle()) < self.EPS:
      #print self.prevSpeed, self.acceleration
      
    previousPieceID = self.getPieceID(self.previousCarData)
    previousLane = self.getLane(self.previousCarData)     
    self.carAngle=self.getCarAngle()
    self.angleVelocity=self.carAngle-self.prevCarAngle    
        
    #BUG: I think it was a bug to have this stuff. Instead I am now using the real data in the line above
    if False:
      alpha  = -self.CONST_A1 * self.angleVelocity
      alpha -= self.CONST_A2 * self.speed * self.carAngle
      
      if self.SLIP_MODEL==1:
        if self.isCornerPiece(pieceID):
          curveRadius=self.getCurveRadius(pieceID,self.getLane())
          
          tmp = (self.speed - self.CONST_R1*curveRadius - self.CONST_R2)
          if (tmp > 0):
            if (self.getPieceAngle(pieceID) < 0):
              alpha -= self.CONST_M * tmp
            else:
              alpha += self.CONST_M * tmp
              
      elif self.SLIP_MODEL==2:                  
        if self.isCornerPiece(pieceID):
          curveRadius=self.getCurveRadius(pieceID,self.getLane())
         
          #tmp = self.speed*self.speed/curveRadius
          tmp = self.speed/curveRadius          
          if (tmp > self.CONST_B1):
            if (self.getPieceAngle(pieceID) < 0):
              alpha -= self.CONST_B2 * (tmp - self.CONST_B1)
            else:
              alpha += self.CONST_B2 * (tmp - self.CONST_B1)    
                                                     
              
      self.angleVelocity += alpha  
      
      
    if self.isCornerPiece(previousPieceID):      
      prevRadius = self.getCurveRadius(previousPieceID, previousLane) 
      #print self.prevCarAngle, self.changeInAngle, prevRadius
                  
    
  def updateAfterThrottle(self):
    self.previousCarData = self.carData      
    self.prevSpeed = self.speed
    self.prevCarAngle = self.carAngle
    self.prevThrottle = self.curThrottle

    
    
  #drive at a constant speed
  #constant speed occurs when T*throttle+V*speed=0
  def AI_constantSpeed(self):
    speed=5
    th=-self.CONST_V*speed/self.CONST_T
    self.throttle(th)
      

############## Simulator ########################
    
    
  # solve every time you are on a new piece or if enough ticks occurred
  # lookAhead=10, step=0.1, minIter=50, maxIter=50, maxTicks=15, maxSlip=60, fastest lap 7.65 + crash ***
  # lookAhead=10, step=0.1, minIter=50, maxIter=50, maxTicks=15, maxSlip=45, fastest lap 7.98 + crash
  # lookAhead=10, step=0.1, minIter=50, maxIter=50, maxTicks=15, maxSlip=30, fastest lap 8.13
  # lookAhead=10, step=0.1, minIter=25, maxIter=25, maxTicks=15, maxSlip=30, fastest lap 8.33
  # lookAhead=15, step=0.1, minIter=50, maxIter=50, maxTicks=15, maxSlip=45, fastest lap 8.52 + crash    
  def AI_simulator(self):
        
    maxTicks=15     #maximum ticks between calls to solve()
    crashLookAhead = self.LOOK_AHEAD/2    #how many pieces to check for crashes?
        
    curPieceID = self.getPieceID()
    previousPieceID = self.getPieceID(self.previousCarData)     
    
    #switch in random places
    nextPieceID = self.getNextPieceID(curPieceID)    
    if self.isSwitchPiece(nextPieceID):
      if random.random() < self.SWITCH_PROBABILITY:
        lane=self.getLane()
        
        #left-most lane, so can only go right
        if lane==0:     
          self.switch('Right')
        #right-most lane, so can only go left
        elif lane==self.getNumLanes()-1:
          self.switch('Left')
        #in the middle, so choose a random direction
        else:
          if random.random() < 0.5:
            self.switch('Left')
          else:
            self.switch('Right')
        #return     
    
      
    #reuse previous throttles
    if curPieceID!=previousPieceID or self.firstCall:
      commands=copy.deepcopy(self.prevCommands)
      commands['th']=commands['th'][1:]
      commands['th'].append(self.STEP)
      self.gametickLastSolve=self.gametick
      commands=self.solve(commands)    
    elif self.gametick-self.gametickLastSolve >= maxTicks:
      commands=copy.deepcopy(self.prevCommands)  
      self.gametickLastSolve=self.gametick      
      commands=self.solve(commands)           
    else:
      commands=self.prevCommands
            

    self.firstCall=False           
    self.prevCommands = copy.deepcopy(commands)
   
    if self.waitingForRespawn:
      # No need to check for crash etc when waiting to re-spawn. just ping
      self.ping()
      return
    
    startState={'piece':self.getPieceID(),
                'pieceDistance':self.getInPieceDistance(),
                'speed':self.speed,
                'angle':self.getCarAngle(),
                'lane':self.getLane(),
                'angleVelocity':self.angleVelocity}    
                
    bestTick=10000

    #simulate a few pieces to see if we will crash
    testCommands=copy.deepcopy(commands)
    testCommands['th']=testCommands['th'][0:crashLookAhead]
    endState=self.simulate(testCommands, startState, bestTick)  
    
    if endState['crash']:
      print('tick',self.gametick,'looks like we will crash...')
      self.throttle(0.0)
    else:
      #activate turbo
      if self.isTurboAvailable and commands['turbo']: self.turbo()

      self.throttle(commands['th'][0])      
      

     

                


  def solve(self, commands):
    #print(self.raceData)
    
    doPrint=True
    n = self.LOOK_AHEAD
    step = self.STEP
    bestTick=10000
    bestScore=1e20
    loadBest=True
    maxMutations=3
    willCrash=True
    
    #0 - no optimization, just rely on random mutations
    #1 - change the value of one throttle
    optimizeType='1'
    
    
    startState={'piece':self.getPieceID(),
                'pieceDistance':self.getInPieceDistance(),
                'speed':self.speed,
                'angle':self.getCarAngle(),
                'lane':self.getLane(),
                'angleVelocity':self.angleVelocity}
                

    if not(self.isTurboAvailable): commands['turbo']=False                
    if self.isTurboActive: commands['turbo']=True
    
    bestCommands=copy.deepcopy(commands)


    numSteps=1/step+1    
    
    
    myiter=0
    while True:
    
      #early termination when we reached time limit
      if self.getTime()-self.OLD_TIME >= self.MAX_TIME:
        break
      
      #load the current best solution
      if loadBest: commands=copy.deepcopy(bestCommands)       
               
      #perform some mutations
      if myiter>0:
        mutations=int(random.random()*maxMutations+1)
        for i in range(mutations):
          pos = int(random.random()*n)
          commands['th'][pos] = step * int(random.random()*numSteps)
      
      
      #possibly perform some optimization on throttles
      if optimizeType=='0':
        endState=self.simulate(commands, startState, bestTick)    
      if optimizeType=='1':
        endState=self.optimize1(startState, commands, n, step)
            
      
      score=self.score(endState)
      #print('tick',self.gametick,'score',score,'bestScore',bestScore)
      
      
      if score<=bestScore:
        if not(endState['crash']):
          bestTick=endState['tick']
          willCrash=False
        
        bestCommands=copy.deepcopy(commands)
        
        self.myRound(commands['th'],2)   #round for nicer output
        
        if score<bestScore and doPrint:
          print('tick',self.gametick,'iter',myiter,'score',score,commands)
            
        bestScore=score        
        
      myiter+=1
      
        
        
    return bestCommands
    
  
  # a move consists of changing the value of one throttle
  def optimize1(self, startState, commands, n, step):
  
    bestTick=10000
    numSteps=int(1/step)+1       
    
    bestState=self.simulate(commands, startState, bestTick)
    bestScore=self.score(bestState)    
    if not bestState['crash']: bestTick=bestState['tick']

    ind = []
    for i in range(n*numSteps+1): ind.append(i)
         
        
    self.shuffle(ind)
    changed=False
      
    for i2 in range(len(ind)):
      
      #early termination when we reached time limit
      if i2%10==0 and self.getTime()-self.OLD_TIME >= self.MAX_TIME:
        break      
        
      i2=ind[i]        
    
      #handle turbo separately
      if i>=n*numSteps:
        
        if self.NO_TURBO: continue                #turbo is switched off!
        if not(self.isTurboAvailable): continue   #no turbo yet
        if self.isTurboActive: continue           #turbo is already happening!   

        #HACK: don't turbo on before curves
        nextPiece=self.getNextPieceID(startState['piece'])
        nextPiece2=self.getNextPieceID(nextPiece)          
        nextPiece3=self.getNextPieceID(nextPiece2)					
        if not(commands['turbo']) and (self.isCornerPiece(nextPiece) or self.isCornerPiece(nextPiece2) or self.isCornerPiece(nextPiece3)):
          continue
        
        commands['turbo']=not(commands['turbo'])
        
        endState=self.simulate(commands, startState, bestTick)        
        score=self.score(endState)
        
        #accept move
        if score<=bestScore:
          if not endState['crash']: bestTick=endState['tick']
          
          if score<bestScore: changed=True
          bestScore=score
          bestState=copy.deepcopy(endState)
        #undo
        else:
          commands['turbo']=not(commands['turbo'])
          
        continue
    
      pos=int(i/numSteps)
      
      #no point optimizing pieces after crash
      if bestState['crash'] and pos > bestState['finishedN']: continue
      
      val=(i%numSteps)*step
      
      if commands['th'][pos]==val: continue    #same value
      
      #no point decreasing throttle if we are not crashing
      if not(bestState['crash']) and val<=commands['th'][pos] - self.STEP:
        continue
                
      #TODO: no point increasing throttle in pieces before the crash?

      oldVal=commands['th'][pos]
      commands['th'][pos]=val
      
      endState=self.simulate(commands, startState, bestTick)        
      score=self.score(endState)
      
      #accept move
      if score<=bestScore:
        if not endState['crash']: bestTick=endState['tick']
        
        if score<bestScore: changed=True
        bestScore=score
        bestState=copy.deepcopy(endState)
      #undo
      else:
        commands['th'][pos]=oldVal
          
      break
      

    return bestState
    
  
    
  
  #round every element in a to some decimals
  def myRound(self, a, decimals):
    for i in range(len(a)): a[i]=round(a[i], decimals)  
      
  
  def score(self, endState):
    return endState['tick']+1000000*endState['piecesLeft']
    
    
  
    
  #TODO:
  # - handle the case where you move multiple pieces in one tick, probably rare
  def simulate(self, commands, startState, maxTicks):
    n=len(commands['th'])
    
    curState=copy.deepcopy(startState)
    #print 'start',curState    
    
    tick=0
    i=0
      
    while i<n and tick<maxTicks:

      tick+=1
      pieceID=curState['piece']
      
      throttle=commands['th'][i]
            
      #use turbo      
      if commands['turbo'] and self.isTurboAvailable:
        if tick<=self.turboDuration:
          throttle=throttle*self.turboFactor               
      if self.isTurboActive:
        if self.gametick+tick-self.turboTick<=self.turboDuration:
          throttle=throttle*self.turboFactor             
      
      if self.isStraightPiece(pieceID):
        pieceLength=self.getPieceLength(pieceID)
      else:
        pieceLength=self.getCurveLength(pieceID,curState['lane'])
      
      if curState['pieceDistance']+curState['speed'] < pieceLength:
        curState['pieceDistance']+=curState['speed']
        curState['speed']+=self.CONST_T*throttle+self.CONST_V*curState['speed']
      else:
        extraDist=curState['pieceDistance']+curState['speed'] - pieceLength
        curState['pieceDistance']=extraDist
        curState['speed']+=self.CONST_T*throttle+self.CONST_V*curState['speed']
        curState['piece']=self.getNextPieceID(curState['piece'])
        i+=1

      # Update the angle and angular velocity

      alpha  = -self.CONST_A1 * curState['angleVelocity']
      alpha -= self.CONST_A2 * curState['speed'] * curState['angle']

      if self.SLIP_MODEL==1:
        if self.isCornerPiece(pieceID):
          curveRadius=self.getCurveRadius(pieceID,curState['lane'])
          
          tmp = (curState['speed'] - self.CONST_R1*curveRadius - self.CONST_R2)
          if (tmp > 0):
            if (self.getPieceAngle(pieceID) < 0):
              alpha -= self.CONST_M * tmp
            else:
              alpha += self.CONST_M * tmp
              
              
      elif self.SLIP_MODEL==2:                  
        if self.isCornerPiece(pieceID):
          curveRadius=self.getCurveRadius(pieceID,curState['lane'])
         
          tmp = curState['speed']/curveRadius         
          #tmp = curState['speed']*curState['speed']/curveRadius
          if (tmp > self.CONST_B1):
            if (self.getPieceAngle(pieceID) < 0):
              alpha -= self.CONST_B2 * (tmp - self.CONST_B1)
            else:
              alpha += self.CONST_B2 * (tmp - self.CONST_B1)   

      curState['angleVelocity'] += alpha
      curState['angle'] += curState['angleVelocity']

      #print tick,curState
      
      #check crashes        
      if self.isCornerPiece(pieceID):
        if self.SLIP_MODEL==3:
          curveRadius=self.getCurveRadius(pieceID,curState['lane'])        
          Vmax=math.sqrt(self.Fslip * curveRadius * self.DANGER_FACTOR)
          if curState['speed'] >= Vmax:
            curState['tick']=tick
            curState['crash']=True
            curState['piecesLeft']=max(1,n-i)      #this includes the current piece, max just in case
            curState['finishedN']=i
            return curState          
        
        else:
          if math.fabs(curState['angle']) > self.MAX_SLIP_ANGLE:
            #print 'CRASH!'
            curState['tick']=tick
            curState['crash']=True
            curState['piecesLeft']=max(1,n-i)      #this includes the current piece, max just in case
            curState['finishedN']=i
            return curState
        
    
    #print "woo!"
    curState['tick']=tick
    curState['crash']=False
    curState['piecesLeft']=n-i
    curState['finishedN']=i
    return curState
    
    
  def shuffle(self, a):
    n=len(a)
    for i in range(n):
      k=int(random.random()*(n-i)+i)
      temp=a[i]
      a[i]=a[k]
      a[k]=temp      
        


if __name__ == "__main__":
  if len(sys.argv) != 5:
    print("Usage: ./run host port botname botkey")
  else:
    host, port, name, key = sys.argv[1:5]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = SmartBot(s, name, key)
    bot.run()
