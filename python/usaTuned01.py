import json
import socket
import sys
import math

class NoobBot(object):

  # TRACK = "finland"
  # TRACK="germany"
  TRACK="usa"
  CAR_COUNT = 2


  def __init__(self, socket, name, key):
    # set some global variables				
    self.socket = socket
    self.name = name
    self.key = key
    self.gametick = 0
    self.haveSwitchedThisPiece = 0
    self.isTurboAvailable = 0
    self.turboActiveTicksRemaining = 0
    self.curThrottle = 1.0				
    self.previousCarData = {}
    self.distanceTravelled = 0
    self.prevSpeed = 0
    self.csvfile = open("stats/usa-stats.csv", "w", encoding="utf-8")
    print('Tick,Lap,Piece,CurveRadius,Speed,CarAngle,Throttle', file=self.csvfile)


  def msg(self, msg_type, data):
    self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.gametick}))

  def send(self, msg):
    self.socket.send((msg + "\n").encode('utf-8'))

  def join(self):
    if self.TRACK == "finland":
      return self.msg("join", {"name": self.name, "key": self.key})
    else:
      return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": self.TRACK, "carCount": self.CAR_COUNT})

  # example: {"msgType": "throttle", "data": 1.0}
  def throttle(self, throttle):
    self.curThrottle = throttle;
    self.msg("throttle", throttle)

	# Displays elapsed seconds of real time and how many real seconds elapsed per game tick (on average)
  def printRealTimeStats(self):
    realTimeEndSeconds = time.time()
    realTimeElapsedSeconds = realTimeEndSeconds - self.realTimeStartSeconds
    realTimeSecondsPerTick = realTimeElapsedSeconds/self.gametick
    print("Real Time performance:", self.gametick, "ticks", realTimeElapsedSeconds, "seconds", realTimeSecondsPerTick, "sec/tick")

  # Turbo multiplies the power of your car's engine for a short period. The server indicates the availability of turbo using the turboAvailable message.
  #
  # {"msgType": "turboAvailable", "data": {
  #   "turboDurationMilliseconds": 500.0,
  #   "turboDurationTicks": 30,
  #   "turboFactor": 3.0
  # }}
  # You can use your turbo boost any time you like after the server has indicated the availability of it. Just like this.
  # 
  # {"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"}
  # Once you turn it on, you can't turn it off. It'll automatically expire in 30 ticks (or what ever the number was in the turboAvailable message).
  def turbo(self):
    if self.isTurboAvailable == 1:
      angle = abs(self.getCarAngle({}))
      if angle < 60:
        self.isTurboAvailable = 0
        print("Turbo engaged! tick=" + str(self.gametick))
        self.msg("turbo", "Vrrrooooooooom!")
      else:
          print("Angle too high for turbo: " + str(angle))
      
  # example: {"msgType": "switchLane", "data": "Left"}    
  def switch(self, mydir):
    if self.haveSwitchedThisPiece == 0:
      self.haveSwitchedThisPiece = 1
      self.msg("switchLane", mydir)
      # print 'switching',mydir

  def ping(self):
    self.msg("ping", {})

  def run(self):
    self.join()
    self.msg_loop()

  def on_join(self, data):
    print("BOT: Joined")
    # print(data)

  def on_game_start(self, data):
    print("BOT: Race started")
    self.throttle(1.0)
      
  def on_game_init(self, data):
    print("BOT: in game init")
    print(data)
      
    # set some global variables        
    self.raceData = data  # contains all data about the race
    
    self.carIdx = self.getCarIndex(self.carId, data['race']['cars'])
      
  def on_crash(self, data):
    print("BOT: Someone crashed")
    self.ping()

  def on_lapFinished(self, data):
    print("BOT: Lap finished")
    print(data)
    self.printRealTimeStats()

  def on_game_end(self, data):
    print("BOT: Race ended")
    self.ping()

  def on_error(self, data):
    print("Error: {0}".format(data))
    self.ping()

  def on_turbo_available(self, data):
    self.isTurboAvailable = 1
    print("Turbo Available!")
#     print("    tick=" + str(self.gametick) + " duration=" + str(data['turboDurationMilliseconds']) + "ms " + str(data['turboDurationTicks']) + " ticks, factor=" + str(data['turboFactor']))
    print("    tick=" + str(self.gametick))

  def on_turbo_start(self, data):
    self.turboActiveTicksRemaining = 30 # FIXME use actual data value here
#     print("    gameTick=" + str(self.gametick) + " duration=" + str(data['turboDurationMilliseconds']) + "ms " + str(data['turboDurationTicks']) + " ticks, factor=" + str(data['turboFactor']))

  def on_yourCar(self, data):
    print("BOT: YourCar:")
    print(data)
    self.carId = data
    self.carIdx = 0

  def msg_loop(self):
    msg_map = {
        'join': self.on_join,
        'yourCar': self.on_yourCar,
        'gameStart': self.on_game_start,
        'gameInit': self.on_game_init,
        'carPositions': self.on_car_positions,
				'lapFinished': self.on_lapFinished,
        'crash': self.on_crash,
        'gameEnd': self.on_game_end,
        'error': self.on_error,
        'turboAvailable': self.on_turbo_available,
        'turboStart': self.on_turbo_start,
    }
    socket_file = s.makefile()
    line = socket_file.readline()
    while line:
      msg = json.loads(line)
      msg_type, data = msg['msgType'], msg['data']
      if 'gameTick' in msg:
        self.gametick = msg['gameTick']
        if self.turboActiveTicksRemaining > 0:
            self.turboActiveTicksRemaining -= 1            
      if msg_type in msg_map:
#         print("Got {0}".format(msg_type) + " tick=" + str(self.gametick) + " th=" + str(self.curThrottle) + " d=" + str(self.distanceTravelled) + " sp=" + str(self.prevSpeed))
        msg_map[msg_type](data)
      else:
        print("Got {0}".format(msg_type) + " tick=" + str(self.gametick) + " time=" + str(self.gametick / 60.0))
        if 'gameTick' in msg:
          self.ping()
      line = socket_file.readline()        
      
### GET METHODS ###
  
  
# raceData #

  def doesPieceHaveAttribute(self, pieceID, attribute):    
    return (attribute in self.raceData['race']['track']['pieces'][pieceID])
            
  def isCornerPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'angle')
    
  def isStraightPiece(self, pieceID):
    return not(self.isCornerPiece(pieceID))
    
  # Additionally, there may be a switch attribute that indicates that this
  # piece is a switch piece, i.e. you can switch lanes here.      
  def isSwitchPiece(self, pieceID):
    return self.doesPieceHaveAttribute(pieceID, 'switch')      
    
  def getPieceAttribute(self, pieceID, attribute):
    return self.raceData['race']['track']['pieces'][pieceID][attribute]
    
  # NOTE: assume piece is a corner piece  
  # The angle attribute is the angle of the circular segment that the
  # piece forms. For left turns, this is a negative value. For a 45-degree
  # bend to the left, the angle would be -45. For a 30-degree turn to the
  # right, this would be 30.    
  def getPieceAngle(self, pieceID):      
    return self.getPieceAttribute(pieceID, 'angle')
              
  def getNextPieceID(self, pieceID):
    return (pieceID + 1) % len(self.raceData['race']['track']['pieces'])  
    
  # For a Bend, there's radius that depicts the radius of the circle
  # that would be formed by this kind of pieces. More specifically,
  # this is the radius at the center line of the track.      
  def getPieceRadius(self, pieceID):
    return self.getPieceAttribute(pieceID, 'radius')
    
  # NOTE: assume lanes are in increasing order of lane index
  # The track may have 1 to 4 lanes. The lanes are described in
  # the protocol as an array of objects that indicate the lane's
  # distance from the track center line. A positive value tells
  # that the lanes is to the right from the center line while a
  # negative value indicates a lane to the left from the center.
  def getLaneDistFromCenter(self, lane):
    return self.raceData['race']['track']['lanes'][lane]['distanceFromCenter']
          
  # NOTE: assume piece is straight
  # For a Straight the length attribute depicts the length of the piece.
  def getPieceLength(self, pieceID):
    return self.getPieceAttribute(pieceID, 'length')
    
# carData #

  # TODO: assumes just 1 player
  # zero based index of the piece the car is on
  def getPieceID(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['pieceIndex']              
    else:
      return data[0]['piecePosition']['pieceIndex']           
          
    
  # TODO: assumes just 1 player      
  # the distance the car's Guide Flag (see above) has travelled from the
  # start of the piece along the current lane    
  # So I guess no need to complex computations for curves?    
  def getInPieceDistance(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['inPieceDistance']          
    else:
      return data[0]['piecePosition']['inPieceDistance']    
    
    
  # TODO: assumes just 1 player     
  # the number of laps the car has completed. The number 0 indicates
  # that the car is on its first lap. The number -1 indicates that it
  # has not yet crossed the start line to begin it's first lap.       
  def getLap(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['lap']      
    else:
      return data[0]['piecePosition']['lap']
             
    
  # TODO: assumes just 1 player, assume startLane==endLane
  # a pair of lane indices. Usually startLaneIndex and endLaneIndex
  # are equal, but they do differ when the car is currently switching lane    
  def getLane(self, data={}):
    if data == {}:
      return self.carData[0]['piecePosition']['lane']['startLaneIndex']      
    else:
      return data[0]['piecePosition']['lane']['startLaneIndex']
    
          
  # TODO: assumes just 1 player  
  # The angle depicts the car's slip angle. Normally this is zero, but
  # when you go to a bend fast enough, the car's tail will start to drift.
  # Naturally, there are limits to how much you can drift without crashing
  # out of the track.            
  def getCarAngle(self, data={}):
    if data == {}:
      return self.carData[0]['angle']                
    else:      
      return data[0]['angle']          
    
    
    
  # distance travelled since last tick
  def getDistanceTravelled(self):  
  
    # start of race, there is no distance travelled yet
    if self.previousCarData == {}: return 0

    curPieceID = self.getPieceID()
    previousPieceID = self.getPieceID(self.previousCarData)
  
    curPieceDistance = self.getInPieceDistance()      
    previousPieceDistance = self.getInPieceDistance(self.previousCarData)
  
    previousLane = self.getLane(self.previousCarData)
  
    distToStartOfCurPiece = curPieceDistance      
    distToStartOfPreviousPiece = previousPieceDistance        
  
    if self.isCornerPiece(previousPieceID):        
      angle = self.getPieceAngle(previousPieceID)
      mult = -1
      if angle < 0: mult = +1  # left turn
      prevPieceLength = (self.getPieceRadius(previousPieceID) + mult * self.getLaneDistFromCenter(previousLane)) * 2 * math.pi * abs(angle) / 360.0
      assert prevPieceLength >= 0
    else:
      prevPieceLength = self.getPieceLength(previousPieceID)
    
    distToEndOfPreviousPiece = prevPieceLength - distToStartOfPreviousPiece        
		#IF THIS ASSERT FAILS then there is something wrong... please record where it fails
		# fails after pre-corner pieceID = 17 on USA (lap=0 tick=282 d=1660.6917330181439) 
		# note server reported piece distance measurements are all based on the centre line-length
		# using lane-length calculations might be causing error.
    #assert distToEndOfPreviousPiece >= 0
  
    # if on the same piece
    if curPieceID == previousPieceID:
      assert distToStartOfCurPiece - distToStartOfPreviousPiece >= 0
      return distToStartOfCurPiece - distToStartOfPreviousPiece
     
    # on a new track piece
    self.haveSwitchedThisPiece = 0
    return distToStartOfCurPiece + distToEndOfPreviousPiece
    

### AI METHODS ###

  def on_car_positions(self, data):
  
    self.carData = data  
    # print(data)
  
    self.updateBeforeThrottle()
    self.AI_simple8a()
    self.updateAfterThrottle()
    
    
  def updateBeforeThrottle(self):
    self.pieceID = self.getPieceID()
    self.lap = self.getLap()
    self.curveRadius = 0
    if self.isCornerPiece(self.pieceID):
      self.curveRadius = self.getPieceRadius(self.pieceID)
    self.carAngle = self.getCarAngle()
    self.dist = self.getDistanceTravelled()  # distance travelled since last tick
    self.distanceTravelled += self.dist      # total distance travelled
    self.speed = self.dist / 1               # instantaneous speed. NOTE: assume we didn't miss any ticks
    self.acceleration = (self.speed - self.prevSpeed) / 1
    if self.gametick == 0:
      self.averageSpeed = 0        
    else:
      self.averageSpeed = self.distanceTravelled / self.gametick  # average speed


#     pieceID = self.getPieceID()      
#     if self.isStraightPiece(pieceID) and self.getCarAngle() == 0:
#       # print "tick",self.gametick,"distance",self.distanceTravelled,"speed",self.speed,"acceleration",self.acceleration
#       print self.speed, self.acceleration
    
    
  def updateAfterThrottle(self):
    print(str(self.gametick) + "," + str(self.lap) + "," + str(self.pieceID) + "," + str(self.curveRadius) + "," + str(self.speed) + "," + str(self.carAngle) + "," + str(self.curThrottle), file=self.csvfile)
    self.previousCarData = self.carData      
    self.prevSpeed = self.speed
      
    
  # Like simple7 but go full throttle at the start of the race and fast across the start/finish line
  # Use Turbo on the final lap home straight sprint.
	#preC=0.14, corner=0.938,           straight=1.00, usa	 time=5.984, 5.350, 5.016 ***
	#preC=0.12, corner=0.93, preS=0.96, straight=1.00, usa	 time=5.834, 5.416, 5.050
	#preC=0.11, corner=0.93, preS=0.95, straight=1.00, usa	 time=5.884, 5.400, 5.066
	#preC=0.10, corner=0.92, preS=0.94, straight=1.00, usa	 time=6.050, 5.434, 5.116
	#preC=0.07, corner=0.91, preS=0.92, straight=1.00, usa	 time=6.234, 5.500, 5.150
	#preC=0.05, corner=0.91, preS=0.91, straight=1.00, usa	 time=6.217, 5.583, 5.184
	#preC=0.00, corner=0.85, preS=0.95, straight=1.00, usa	 time=6.467, 5.717, 5.250
  def AI_simple8(self):
    
    preCornerThrottle = 0.179 # 0.14
    cornerThrottle = 0.939 # 0.939
    straightThrottle = 1.0
    
    pieceID = self.getPieceID()
    nextPieceID = self.getNextPieceID(pieceID)  
   
		# switch lanes if it is faster.
    if self.isSwitchPiece(nextPieceID):
      nextPieceID2 = self.getNextPieceID(nextPieceID)
      if self.isCornerPiece(nextPieceID2):
        # next piece turns left					
        if self.getPieceAngle(nextPieceID2) < 0:	 
          self.switch('Left')
        else:
          self.switch('Right')

    # full throttle at the start
    if self.gametick < 127:
#       print("race start!")
      self.throttle(1.0)
      return


    # Turbo and full throttle at the final finish (-7 finland, -5 usa)
    if self.getLap()==2 and pieceID>=len(self.raceData['race']['track']['pieces'])-5:
#       print("final finish turbo sprint " + str(pieceID))
      self.turbo()
      self.throttle(1.0)
      return

    if self.isCornerPiece(pieceID):
#       print("corner " + str(pieceID))
      self.throttle(cornerThrottle)
      return
    
    if self.isCornerPiece(nextPieceID):
#       print("preCorner " + str(pieceID))
      self.throttle(preCornerThrottle)
    else:
#       print("straight " + str(pieceID))
#       self.turbo() # causes crash at end of straight when combined with high throttle!
      self.throttle(straightThrottle)          


	# Like simple8a but without any lane switching
    #preC=0.98, corner=1.00,                     straight=1.00, usa  time=4.73
	#preC=0.18, corner=0.94,					 straight=1.00, usa	 time=5.45
  def AI_simple8a(self):
		
    preCornerThrottle = 1.0 # 0.997
    cornerThrottle = 1.0
    straightThrottle = 1.0
		
    pieceID = self.getPieceID()
    nextPieceID = self.getNextPieceID(pieceID)	
	
    turboPiece = 12
    brakePiece = turboPiece + 3
    if self.gametick > 700 and pieceID >= turboPiece and pieceID < brakePiece:
      self.turbo()
      self.throttle(1.0)
      print("Turbo tick=" + str(self.gametick) + " th=" + str(self.curThrottle) + " p=" + str(pieceID) + " sp=" + str(self.prevSpeed))
      return

    if self.gametick > 700 and pieceID >= brakePiece and self.prevSpeed > 10.4:
      self.throttle(0.0)
      print("Brake tick=" + str(self.gametick) + " th=" + str(self.curThrottle) + " p=" + str(pieceID) + " sp=" + str(self.prevSpeed))
      return
     
#     if self.gametick >= brakeTick and self.prevSpeed > 10.4:
#       self.throttle(0.151)
#       print("Half-Brake tick=" + str(self.gametick) + " th=" + str(self.curThrottle) + " p=" + str(pieceID) + " sp=" + str(self.prevSpeed))
#       return
		
#     # Turbo and full throttle at the final finish (-7 finland, -5 usa)
#     if self.getLap()==2 and pieceID>=len(self.raceData['race']['track']['pieces'])-5:
# #     print("final finish turbo sprint " + str(pieceID))
#       self.turbo()
#       angle = abs(self.getCarAngle({}))
#       if angle > 25 and self.turboActiveTicksRemaining > 0:
#           self.throttle(1.1 - 0.01 * angle)
#       else:
#           self.throttle(1.0)
#       return

    if self.isCornerPiece(pieceID):
#     print("corner " + str(pieceID))
      self.throttle(cornerThrottle)
      return
		
    if self.isCornerPiece(nextPieceID):
#     print("preCorner " + str(pieceID))
      self.throttle(preCornerThrottle)
    else:
#     print("straight " + str(pieceID))
#     self.turbo() # causes crash at end of straight when combined with high throttle!
      self.throttle(straightThrottle)					


if __name__ == "__main__":
  if len(sys.argv) != 5:
    print("Usage: ./run host port botname botkey")
  else:
    host, port, name, key = sys.argv[1:5]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = NoobBot(s, name, key)
    bot.run()
